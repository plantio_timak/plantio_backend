#!/bin/bash
# TODO : remove login from gitlab ci
#echo "Logging in to Docker registry"
#docker login registry.gitlab.com
echo "Stopping runnning container"
docker-compose down
echo "Removing old image"
docker rmi registry.gitlab.com/plantio_timak/plantio_backend:latest
echo "Fetching the latest image"
docker pull registry.gitlab.com/plantio_timak/plantio_backend:latest
echo "Starting new docker container"
docker-compose up -d