
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema plantIO_DB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema plantIO_DB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `plantIO_DB` DEFAULT CHARACTER SET utf8 ;
USE `plantIO_DB` ;

-- -----------------------------------------------------
-- Table `plantIO_DB`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `plantIO_DB`.`user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(45) NULL,
    `password` VARCHAR(80) NULL,
    `email` VARCHAR(45) NULL,
    `last_used_service_instance_id` INT NULL,
    `last_used_service_id` INT NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `plantIO_DB`.`service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `plantIO_DB`.`service` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `plantIO_DB`.`service_instance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `plantIO_DB`.`service_instance` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `ip` VARCHAR(45) NULL,
    `name` VARCHAR(45) NULL,
    `service_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_service_instance_service1_idx` (`service_id` ASC) VISIBLE,
    CONSTRAINT `fk_service_instance_service1`
    FOREIGN KEY (`service_id`)
    REFERENCES `plantIO_DB`.`service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `plantIO_DB`.`user_service_instance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `plantIO_DB`.`user_service_instance` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `service_instance_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_user_service_instance_user_idx` (`user_id` ASC) VISIBLE,
    INDEX `fk_user_service_instance_service_instance1_idx` (`service_instance_id` ASC) VISIBLE,
    CONSTRAINT `fk_user_service_instance_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `plantIO_DB`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_user_service_instance_service_instance1`
    FOREIGN KEY (`service_instance_id`)
    REFERENCES `plantIO_DB`.`service_instance` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `plantIO_DB`.`watering_mode`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `plantIO_DB`.`watering_mode` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NULL,
    `min_soil_humidity` INT NULL,
    `min_soil_temperature` INT NULL,
    `min_air_humidity` INT NULL,
    `min_air_temperature` INT NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `plantIO_DB`.`device`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `plantIO_DB`.`device` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(45) NULL,
    `plant` VARCHAR(45) NULL,
    `use_interactive_mode` TINYINT NULL,
    `use_time_mode` TINYINT NULL,
    `time_mode_hours` VARCHAR(45) NULL,
    `watering_mode_id` INT NOT NULL,
    `service_instance_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_device_watering_mode1_idx` (`watering_mode_id` ASC) VISIBLE,
    INDEX `fk_device_service_instance1_idx` (`service_instance_id` ASC) VISIBLE,
    CONSTRAINT `fk_device_watering_mode1`
    FOREIGN KEY (`watering_mode_id`)
    REFERENCES `plantIO_DB`.`watering_mode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_device_service_instance1`
    FOREIGN KEY (`service_instance_id`)
    REFERENCES `plantIO_DB`.`service_instance` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
