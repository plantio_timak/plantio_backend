package sk.timak.plantio;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockedStatic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sk.timak.plantio.conf.TestContext;
import sk.timak.plantio.conf.jwt.JWTUtils;
import sk.timak.plantio.errors.PlantioErrorCode;
import sk.timak.plantio.influxdb.InfluxDbHandler;
import sk.timak.plantio.persistance.respository.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;
import static sk.timak.plantio.conf.jwt.JWTUtils.generateTokenAfterSuccessLogin;
import static sk.timak.plantio.conf.jwt.JWTUtils.getUserNameFromRequest;
import static sk.timak.plantio.utils.TestUtils.*;

@AutoConfigureMockMvc
@ContextConfiguration(classes = TestContext.class)
public abstract class PlantioApplicationTests {

    protected static Logger LOGGER = LoggerFactory.getLogger(PlantioApplicationTests.class);
    protected static String sessionId;

    protected MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected JWTUtils jwtUtils;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected UserServiceInstanceRepository userServiceInstanceRepository;

    @Autowired
    protected ServiceRepository serviceRepository;

    @Autowired
    protected DeviceRepository deviceRepository;

    @Autowired
    protected ServiceInstanceRepository serviceInstanceRepository;

    @Autowired
    protected WateringModeRepository wateringModeRepository;

    @Autowired
    protected InfluxDbHandler influxDbHandler;

    MockedStatic<JWTUtils> mockedJwtUtils;

    @BeforeEach
    protected void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();

        when(influxDbHandler.selectDataFromInfluxByPlantId(any())).thenReturn(getMockedQueryResultForPlant());

        this.mockedJwtUtils = mockStatic(JWTUtils.class);
        this.mockedJwtUtils.when(() -> getUserNameFromRequest(any())).thenReturn("test");
        this.mockedJwtUtils.when(() -> generateTokenAfterSuccessLogin(any())).thenReturn("token");
        when(userRepository.getAllUsers()).thenReturn(getMockedUserList());

        sessionId = generateSessionIdForTest();
        LOGGER.info("Generated sessionId: {}", sessionId);
    }

    @AfterEach
    protected void close() {
        this.mockedJwtUtils.close();
    }


    protected void assertErrors(JsonNode root, PlantioErrorCode errorCode, String scope) {
        assertThat(root.get("errors").isArray()).isTrue();
        assertThat(root.get("errors").get(0).get("error").asText()).isEqualTo(errorCode.toString());
        assertThat(root.get("errors").get(0).get("scope").asText()).isEqualTo(scope);
    }
}
