package sk.timak.plantio.conf;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import sk.timak.plantio.conf.jwt.JWTUtils;
import sk.timak.plantio.conf.jwt.JwtAuthenticationEntryPoint;
import sk.timak.plantio.conf.mqtt.MqttConnection;
import sk.timak.plantio.exception.handler.PlantioExceptionHandler;
import sk.timak.plantio.impl.AdminServiceImpl;
import sk.timak.plantio.impl.PlantServiceImpl;
import sk.timak.plantio.impl.RoomServiceImpl;
import sk.timak.plantio.impl.UserServiceImpl;
import sk.timak.plantio.influxdb.InfluxDbHandler;
import sk.timak.plantio.persistance.respository.*;
import sk.timak.plantio.service.AdminService;
import sk.timak.plantio.service.PlantService;
import sk.timak.plantio.service.RoomService;
import sk.timak.plantio.service.UserService;

import javax.sql.DataSource;
import java.util.Properties;

import static org.mockito.Mockito.mock;


@Configuration
@Import({WebConfiguration.class, WebSecurityConfiguration.class})
@EntityScan("sk.plantio.model.enitity")
@ComponentScan(basePackages = "sk.timak.plantio.web")
public class TestContext {


    @Bean
    public JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint() {
        return new JwtAuthenticationEntryPoint();
    }

    @Bean
    public JWTUtils jwtUtils(){
        return mock(JWTUtils.class);
    }


    @Bean
    public PlantioExceptionHandler plantioExceptionHandler() {
        return new PlantioExceptionHandler();
    }

    @Bean
    @Primary
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .generateUniqueName(true)
                .setType(EmbeddedDatabaseType.HSQL)
                .setScriptEncoding("UTF-8")
                .ignoreFailedDrops(true)
                .addScript("plantio-test.sql")
                .build();
    }

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(jpaVendorAdapter());
        Properties jpaProperties = new Properties();
        factory.setJpaProperties(jpaProperties);
        factory.setPackagesToScan("sk.timak.plantio.model.entity");
        factory.setDataSource(this.dataSource());
        return factory;
    }

    @Bean
    @Primary
    public HibernateJpaVendorAdapter jpaVendorAdapter(){
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.HSQL);
        adapter.setGenerateDdl(true);
        return adapter;
    }

    @Bean
    public UserService userService(){
        return new UserServiceImpl(userRepository(), userServiceInstanceRepository(),
                serviceInstanceRepository(), serviceRepository());
    }

    @Bean
    public AdminService adminService(){
        return new AdminServiceImpl(serviceInstanceRepository(), serviceRepository(), deviceRepository());
    }

    @Bean
    public PlantService plantService(){
        return new PlantServiceImpl(deviceRepository(), userServiceInstanceRepository(), wateringModeRepository(), influxDbHandler(), mqttConnection());
    }

    @Bean
    public RoomService roomService(){
        return new RoomServiceImpl(serviceInstanceRepository(), userServiceInstanceRepository(),
                userRepository(), serviceRepository(), influxDbHandler());
    }

    @Bean
    protected UserRepository userRepository(){
        return mock(UserRepository.class);
    }

    @Bean
    protected UserServiceInstanceRepository userServiceInstanceRepository(){
        return mock(UserServiceInstanceRepository.class);
    }
    @Bean
    protected ServiceInstanceRepository serviceInstanceRepository(){
        return mock(ServiceInstanceRepository.class);
    }

    @Bean
    protected ServiceRepository serviceRepository(){
        return mock(ServiceRepository.class);
    }

    @Bean
    protected DeviceRepository deviceRepository(){
        return mock(DeviceRepository.class);
    }

    @Bean
    protected WateringModeRepository wateringModeRepository(){
        return mock(WateringModeRepository.class);
    }

    @Bean
    public InfluxDbHandler influxDbHandler() {
        return mock(InfluxDbHandler.class);
    }

    @Bean
    public MqttConnection mqttConnection() { return mock(MqttConnection.class); }

    @Bean
    public PlantServiceImpl plantServiceImpl() { return mock(PlantServiceImpl.class); }

}
