package sk.timak.plantio.utils;

import org.apache.commons.io.*;
import org.influxdb.dto.QueryResult;
import org.springframework.core.io.ClassPathResource;
import sk.timak.plantio.interfaces.ServiceType;
import sk.timak.plantio.model.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class TestUtils {


    public static String loadJsonResource(String fileName) throws Exception {
        return loadResourceAsString("json", fileName);
    }

    private static String loadResourceAsString(String subfolder, String fileName) throws Exception {
        return FileUtils.readFileToString((new ClassPathResource(String.format("%s/%s", subfolder, fileName))).getFile(), "UTF-8");
    }

    public static String generateSessionIdForTest() {
        return String.valueOf(UUID.randomUUID());
    }

    public static List<User> getMockedUserList() {
        User user1 = new User();
        user1.setId(1);
        user1.setUserName("test");
        user1.setPassword("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08");
        user1.setEmail("email@email.sk");
        user1.setLastUsedServiceId(1);
        user1.setLastUsedServiceInstanceId(10);

        User user2 = new User();
        user2.setId(2);
        user2.setUserName("test2");
        user2.setPassword("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08");
        user2.setEmail("email@email.sk");
        user2.setLastUsedServiceId(1);
        user2.setLastUsedServiceInstanceId(20);

        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        return users;
    }

    public static List<UserServiceInstance> getMockedUserServiceInstance() {
        UserServiceInstance userServiceInstance1 = new UserServiceInstance();
        userServiceInstance1.setId(1);
        userServiceInstance1.setServiceInstanceId(1);
        userServiceInstance1.setUserId(1);

        UserServiceInstance userServiceInstance2 = new UserServiceInstance();
        userServiceInstance2.setId(2);
        userServiceInstance2.setServiceInstanceId(2);
        userServiceInstance2.setUserId(1);

        List<UserServiceInstance> userServiceInstances = new ArrayList<>();
        userServiceInstances.add(userServiceInstance1);
        userServiceInstances.add(userServiceInstance2);
        return userServiceInstances;
    }

    public static List<Service> getMockedServices() {
        Service service = new Service();
        service.setId(1);
        service.setName(ServiceType.PLANTS_MODULE);
        List<Service> services = new ArrayList<>();
        services.add(service);
        return services;
    }

    public static List<ServiceInstance> getMockedServiceInstances() {
        ServiceInstance serviceInstance1 = new ServiceInstance();
        serviceInstance1.setId(1);
        serviceInstance1.setIp("123.456.789");
        serviceInstance1.setName("obyvacka");
        serviceInstance1.setServiceId(1);

        ServiceInstance serviceInstance2 = new ServiceInstance();
        serviceInstance2.setId(2);
        serviceInstance2.setIp("123.456.789");
        serviceInstance2.setName("kuchyna");
        serviceInstance2.setServiceId(1);

        ServiceInstance serviceInstance3 = new ServiceInstance();
        serviceInstance3.setId(3);
        serviceInstance3.setIp("123.456.789");
        serviceInstance3.setName("kuchyna");
        serviceInstance3.setServiceId(1);

        ServiceInstance serviceInstance15 = new ServiceInstance();
        serviceInstance15.setId(15);
        serviceInstance15.setIp("123.456.789");
        serviceInstance15.setName("kuchyna");
        serviceInstance15.setServiceId(1);

        List<ServiceInstance> serviceInstances = new ArrayList<>();
        serviceInstances.add(serviceInstance1);
        serviceInstances.add(serviceInstance2);
        serviceInstances.add(serviceInstance3);
        serviceInstances.add(serviceInstance15);
        return serviceInstances;
    }

    public static List<Long> getMockedUserServiceInstances() {
        return getMockedServiceInstances().stream()
                .map(ServiceInstance::getId)
                .collect(Collectors.toList());
    }

    public static List<Device> getMockedDevices() {

        Device device1 = new Device();
        device1.setId(1);
        device1.setType("kvetinac pre paradajku");
        device1.setPlant("paradajka");
        device1.setTimeModeHours(1);
        device1.setUseTimeMode(true);
        device1.setUseInteractiveMode(false);
        device1.setWateringModeId(1);
        device1.setServiceInstanceId(1);

        Device device2 = new Device();
        device2.setId(2);
        device2.setType("kvetinac pre uhorku");
        device2.setPlant("uhorka");
        device2.setTimeModeHours(2);
        device2.setUseTimeMode(false);
        device2.setUseInteractiveMode(true);
        device2.setWateringModeId(1);
        device2.setServiceInstanceId(1);

        List<Device> devices = new ArrayList<>();
        devices.add(device1);
        devices.add(device2);

        return devices;
    }


    public static List<WateringMode> getMockedWateringModes() {

        WateringMode wateringMode1 = new WateringMode();
        wateringMode1.setId(1);
        wateringMode1.setName("paprika");
        wateringMode1.setMinAirHumidity(70);
        wateringMode1.setMinAirTemperature(20);
        wateringMode1.setMinSoilHumidity(40);
        wateringMode1.setMinSoilTemperature(15);

        WateringMode wateringMode2 = new WateringMode();
        wateringMode2.setId(2);
        wateringMode2.setName("uhorka");
        wateringMode2.setMinAirHumidity(60);
        wateringMode2.setMinAirTemperature(25);
        wateringMode2.setMinSoilHumidity(50);
        wateringMode2.setMinSoilTemperature(20);

        List<WateringMode> wateringModes = new ArrayList<>();
        wateringModes.add(wateringMode1);
        wateringModes.add(wateringMode2);

        return wateringModes;
    }

    public static Optional<QueryResult.Result> getMockedQueryResultForRoom() {
        QueryResult.Result queryResult = new QueryResult.Result();
        List<QueryResult.Series> series = new ArrayList<>();
        QueryResult.Series serie = new QueryResult.Series();
        serie.setColumns(List.of( "time","humidity","light_intensity","room_id","temperature"));
        serie.setValues(List.of(List.of("2021-03-31T19:57:12.654Z","74.2","82","1","24"), List.of("2021-03-31T21:57:12.654Z","74.5","85","1","23.2")));
        series.add(0,serie);
        queryResult.setSeries(series);

        return Optional.of(queryResult);
    }
    public static Optional<QueryResult.Result> getMockedQueryResultForPlant() {
        QueryResult.Result queryResult = new QueryResult.Result();
        List<QueryResult.Series> series = new ArrayList<>();
        QueryResult.Series serie = new QueryResult.Series();
        serie.setColumns(List.of( "time","humidity","plant_id","temperature"));
        serie.setValues(List.of(List.of("2021-03-31T19:57:12.654Z","74.2","1","24"), List.of("2021-03-31T21:57:12.654Z","74.5","1","23.2")));
        series.add(0,serie);
        queryResult.setSeries(series);

        return Optional.of(queryResult);
    }

}
