package sk.timak.plantio;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import sk.timak.plantio.conf.SecurityConstants;
import sk.timak.plantio.model.plant.PlantMode;
import sk.timak.plantio.model.room.GraphInterval;
import sk.timak.plantio.web.PlantController;
import sk.timak.plantio.web.path.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sk.timak.plantio.errors.PlantioErrorCode.*;
import static sk.timak.plantio.utils.TestUtils.*;
import static sk.timak.plantio.web.PlantController.USER_PLANTS;
import static sk.timak.plantio.web.PlantController.WATERING_MODES;

@WebMvcTest(PlantController.class)
public class PlantControllerResponsesTests extends PlantioApplicationTests {


    public PlantControllerResponsesTests() {
    }

    @Test
    public void testGetUserPlants() throws Exception {

        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(userServiceInstanceRepository.getUserRooms(anyLong())).thenReturn(getMockedUserServiceInstances());
        when(deviceRepository.getExistingDevices()).thenReturn(getMockedDevices());
        when(wateringModeRepository.getAllWateringModes()).thenReturn(getMockedWateringModes());

        MvcResult result = mockMvc.perform(
                get(Path.WATERING + USER_PLANTS, "1")
                        .param("interval", GraphInterval.DAY.toString())
                        .header(SecurityConstants.HEADER_STRING, sessionId))
                .andExpect(status().isOk())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);


        assertThat(root.get("plants").isArray()).isTrue();
        assertThat(root.get("plants").size()).isEqualTo(2);
        assertThat(root.get("plants").get(0).get("id").asLong()).isEqualTo(1);
        assertThat(root.get("plants").get(0).get("name").asText()).isEqualTo("kvetinac pre paradajku");
        assertThat(root.get("plants").get(0).get("plant").asText()).isEqualTo("paradajka");
        assertThat(root.get("plants").get(0).get("mode").get("plantMode").asText()).isEqualTo(PlantMode.TIME.toString());
        assertThat(root.get("plants").get(0).get("mode").get("time").asText()).isEqualTo("1");

        assertThat(root.get("plants").get(1).get("id").asLong()).isEqualTo(2);
        assertThat(root.get("plants").get(1).get("name").asText()).isEqualTo("kvetinac pre uhorku");
        assertThat(root.get("plants").get(1).get("plant").asText()).isEqualTo("uhorka");
        assertThat(root.get("plants").get(1).get("mode").get("plantMode").asText()).isEqualTo(PlantMode.INTERACTIVE.toString());
        assertThat(root.get("plants").get(1).get("wateringMode").get("id").asLong()).isEqualTo(1);
        assertThat(root.get("plants").get(1).get("wateringMode").get("name").asText()).isEqualTo("paprika");
    }

    @Test
    public void testGetUserPlantsWrongRoomId() throws Exception {

        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(userServiceInstanceRepository.getUserRooms(anyLong())).thenReturn(getMockedUserServiceInstances());

        MvcResult result = mockMvc.perform(
                get(Path.WATERING + USER_PLANTS, "4")
                        .param("interval", GraphInterval.DAY.toString())
                        .header(SecurityConstants.HEADER_STRING, sessionId))
                .andExpect(status().is4xxClientError())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertErrors(root, ROOM_NOT_EXIST_FOR_USER, "4");
    }


    @Test
    public void testGetWateringModes() throws Exception {

        when(wateringModeRepository.getAllWateringModes()).thenReturn(getMockedWateringModes());

        MvcResult result = mockMvc.perform(
                get(Path.WATERING + WATERING_MODES)
                        .header(SecurityConstants.HEADER_STRING, sessionId))
                .andExpect(status().isOk())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertThat(root.get("wateringModes").isArray()).isTrue();
        assertThat(root.get("wateringModes").size()).isEqualTo(2);
        assertThat(root.get("wateringModes").get(0).get("id").asLong()).isEqualTo(1);
        assertThat(root.get("wateringModes").get(0).get("name").asText()).isEqualTo("paprika");
        assertThat(root.get("wateringModes").get(1).get("id").asLong()).isEqualTo(2);
        assertThat(root.get("wateringModes").get(1).get("name").asText()).isEqualTo("uhorka");

    }


    @Test
    public void testChangePlantMode() throws Exception {

        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(userServiceInstanceRepository.getUserRooms(anyLong())).thenReturn(getMockedUserServiceInstances());
        when(deviceRepository.getExistingDevices()).thenReturn(getMockedDevices());
        when(wateringModeRepository.getAllWateringModes()).thenReturn(getMockedWateringModes());

        final String request = loadJsonResource("change-plant-mode-request.json");

        MvcResult result = mockMvc.perform(
                put(Path.WATERING + "/my/plant/1/plant-mode")
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertThat(root.get("id").asLong()).isEqualTo(1);
        assertThat(root.get("name").asText()).isEqualTo("kvetinac pre paradajku");
        assertThat(root.get("plant").asText()).isEqualTo("paradajka");
        assertThat(root.get("mode").get("plantMode").asText()).isEqualTo(PlantMode.INTERACTIVE.toString());
        assertThat(root.get("wateringMode").get("id").asLong()).isEqualTo(2);
        assertThat(root.get("wateringMode").get("name").asText()).isEqualTo("uhorka");
    }

    @Test
    public void testChangePlantModeWrongPlantId() throws Exception {

        when(deviceRepository.getExistingDevices()).thenReturn(getMockedDevices());

        final String request = loadJsonResource("change-plant-mode-request.json");

        MvcResult result = mockMvc.perform(
                put(Path.WATERING + "/my/plant/5/plant-mode")
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertErrors(root, ID_NOT_FOUND, "plantId");

    }

    @Test
    public void testChangePlantName() throws Exception {

        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(userServiceInstanceRepository.getUserRooms(anyLong())).thenReturn(getMockedUserServiceInstances());
        when(deviceRepository.getExistingDevices()).thenReturn(getMockedDevices());
        when(wateringModeRepository.getAllWateringModes()).thenReturn(getMockedWateringModes());

        final String request = loadJsonResource("change-plant-name-request.json");

        MvcResult result = mockMvc.perform(
                put(Path.WATERING + "/my/plant/1")
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertThat(root.get("id").asLong()).isEqualTo(1);
        assertThat(root.get("name").asText()).isEqualTo("kvetinac");
        assertThat(root.get("plant").asText()).isEqualTo("uhorka");
        assertThat(root.get("mode").get("plantMode").asText()).isEqualTo(PlantMode.TIME.toString());
        assertThat(root.get("mode").get("time").asText()).isEqualTo("1");
    }

    @Test
    public void testChangePlantNameFieldTooLongException() throws Exception {

        final String request = loadJsonResource("change-plant-name-request-name-too-long.json");

        MvcResult result = mockMvc.perform(
                put(Path.WATERING + "/my/plant/1")
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertErrors(root, FIELD_TOO_LONG, "name");
    }

}


