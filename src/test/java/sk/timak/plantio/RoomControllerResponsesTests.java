package sk.timak.plantio;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import sk.timak.plantio.conf.SecurityConstants;
import sk.timak.plantio.model.plant.PlantMode;
import sk.timak.plantio.web.RoomController;
import sk.timak.plantio.web.path.Path;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sk.timak.plantio.errors.PlantioErrorCode.*;
import static sk.timak.plantio.interfaces.ServiceType.PLANTS_MODULE;
import static sk.timak.plantio.utils.TestUtils.*;
import static sk.timak.plantio.web.RoomController.*;

@WebMvcTest(RoomController.class)
public class RoomControllerResponsesTests extends PlantioApplicationTests {


    public RoomControllerResponsesTests() {
    }

    @Test
    public void testGetUserRooms() throws Exception {

        when(serviceRepository.getAllServices()).thenReturn(getMockedServices());
        when(serviceInstanceRepository.getAllServiceInstances()).thenReturn(getMockedServiceInstances());
        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(userServiceInstanceRepository.getUserRooms(anyLong())).thenReturn(getMockedUserServiceInstances());

        MvcResult result = mockMvc.perform(
                get(Path.WATERING + USER_ROOMS)
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .param("serviceType", PLANTS_MODULE.toString()))
                .andExpect(status().isOk())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertUserRooms(root);
    }

    @Test
    public void testAddRoomToUser() throws Exception {

        when(serviceRepository.getAllServices()).thenReturn(getMockedServices());
        when(serviceInstanceRepository.getAllServiceInstances()).thenReturn(getMockedServiceInstances());
        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(userServiceInstanceRepository.getUserRooms(anyLong())).thenReturn(getMockedUserServiceInstances());

        final String request = format(loadJsonResource("add-room-request.json"), "3");

        mockMvc.perform(
                post(Path.WATERING + ADD_ROOM_TO_USER)
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testAddRoomToUserIdNotFound() throws Exception {

        when(serviceInstanceRepository.getAllServiceInstances()).thenReturn(getMockedServiceInstances());

        final String request = format(loadJsonResource("add-room-request.json"), "7");

        MvcResult result = mockMvc.perform(
                post(Path.WATERING + ADD_ROOM_TO_USER)
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);
        assertErrors(root, ID_NOT_FOUND, "deviceId");
    }

    @Test
    public void testAddRoomToUserDeviceAlreadyAssigned() throws Exception {

        when(serviceRepository.getAllServices()).thenReturn(getMockedServices());
        when(serviceInstanceRepository.getAllServiceInstances()).thenReturn(getMockedServiceInstances());
        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(userServiceInstanceRepository.getUserRooms(anyLong())).thenReturn(getMockedUserServiceInstances());

        final String request = format(loadJsonResource("add-room-request.json"), "1");

        MvcResult result = mockMvc.perform(
                post(Path.WATERING + ADD_ROOM_TO_USER)
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);
        assertErrors(root, DEVICE_ALREADY_ASSIGNED, "device");
    }


    @Test
    public void testChangeRoomName() throws Exception {

        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(userServiceInstanceRepository.getUserRooms(anyLong())).thenReturn(getMockedUserServiceInstances());
        when(serviceInstanceRepository.getAllServiceInstances()).thenReturn(getMockedServiceInstances());
        when(serviceRepository.getAllServices()).thenReturn(getMockedServices());

        final String request = loadJsonResource("change-room-name-request.json");

        MvcResult result = mockMvc.perform(
                put(Path.WATERING + "/my/room/1")
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .param("serviceType", PLANTS_MODULE.toString())
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertThat(root.get("userRooms").isArray()).isTrue();
        assertThat(root.get("userRooms").size()).isEqualTo(2);
        assertThat(root.get("userRooms").get(0).get("id").asLong()).isEqualTo(1);
        assertThat(root.get("userRooms").get(0).get("name").asText()).isEqualTo("jedalen");
        assertThat(root.get("userRooms").get(1).get("id").asLong()).isEqualTo(2);
        assertThat(root.get("userRooms").get(1).get("name").asText()).isEqualTo("kuchyna");
    }

    @Test
    public void testChangeRoomNameFieldTooLongException() throws Exception {

        final String request = loadJsonResource("change-room-name-request-field-too-long.json");

        MvcResult result = mockMvc.perform(
                put(Path.WATERING + "/my/room/1")
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .param("serviceType", PLANTS_MODULE.toString())
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertErrors(root, FIELD_TOO_LONG, "name");
    }

    private void assertUserRooms(JsonNode root){
        assertThat(root.get("userRooms").isArray()).isTrue();
        assertThat(root.get("userRooms").size()).isEqualTo(2);
        assertThat(root.get("userRooms").get(0).get("id").asLong()).isEqualTo(1);
        assertThat(root.get("userRooms").get(0).get("name").asText()).isEqualTo("obyvacka");
        assertThat(root.get("userRooms").get(1).get("id").asLong()).isEqualTo(2);
        assertThat(root.get("userRooms").get(1).get("name").asText()).isEqualTo("kuchyna");
    }
}
