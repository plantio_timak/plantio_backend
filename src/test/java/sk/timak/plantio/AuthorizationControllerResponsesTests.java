package sk.timak.plantio;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import sk.timak.plantio.web.AuthorizationController;
import sk.timak.plantio.web.path.Path;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sk.timak.plantio.errors.PlantioErrorCode.FIELD_INVALID;
import static sk.timak.plantio.interfaces.ServiceType.PLANTS_MODULE;
import static sk.timak.plantio.utils.TestUtils.*;

@WebMvcTest(AuthorizationController.class)
public class AuthorizationControllerResponsesTests extends PlantioApplicationTests {


    public AuthorizationControllerResponsesTests() {
    }

    @Test
    public void testLoginUser() throws Exception {

        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(serviceRepository.getAllServices()).thenReturn(getMockedServices());
        when(serviceInstanceRepository.getAllServiceInstances()).thenReturn(getMockedServiceInstances());

        final String request = format(loadJsonResource("login-user-request.json"), "test");

        MvcResult result = mockMvc.perform(
                post(Path.LOGIN + Path.AUTHORIZATION)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertThat(root.get("userName").textValue()).isEqualTo("test");
        assertThat(root.get("token").asText()).isEqualTo("token");
        assertThat(root.get("email").textValue()).isEqualTo("email@email.sk");
        assertThat(root.get("services").isArray()).isTrue();
        assertThat(root.get("services").get(0).textValue()).isEqualTo(PLANTS_MODULE.toString());
        assertThat(root.get("lastUsedService").asText()).isEqualTo(PLANTS_MODULE.toString());
        assertThat(root.get("lastUsedServiceInstance").asLong()).isEqualTo(0);
    }

    @Test
    public void testLoginUserWrongPassword() throws Exception {

        final String request = format(loadJsonResource("login-user-request.json"), "wrongpassword");

        MvcResult result = mockMvc.perform(
                post(Path.LOGIN + Path.AUTHORIZATION)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        final String payload = result.getResponse().getContentAsString();
        final JsonNode root = new ObjectMapper().readTree(payload);

        assertErrors(root, FIELD_INVALID, "userName or password");
    }
}
