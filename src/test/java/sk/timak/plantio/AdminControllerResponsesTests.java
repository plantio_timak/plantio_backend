package sk.timak.plantio;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import sk.timak.plantio.conf.SecurityConstants;
import sk.timak.plantio.web.AdminController;
import sk.timak.plantio.web.path.Path;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sk.timak.plantio.utils.TestUtils.*;

@WebMvcTest(AdminController.class)
public class AdminControllerResponsesTests extends PlantioApplicationTests {


    public AdminControllerResponsesTests() {
    }

    @Test
    public void testCreateRoomForDevice() throws Exception {

        when(userServiceInstanceRepository.getUserServiceInstancesById(anyLong())).thenReturn(getMockedUserServiceInstance());
        when(serviceRepository.getAllServices()).thenReturn(getMockedServices());
        when(serviceInstanceRepository.getAllServiceInstances()).thenReturn(getMockedServiceInstances());

        final String request = loadJsonResource("create-room-for-device-request.json");

        mockMvc.perform(
                post(Path.ADMIN + Path.CREATE_ROOM)
                        .header(SecurityConstants.HEADER_STRING, sessionId)
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }
}
