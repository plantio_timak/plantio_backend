package sk.timak.plantio.influxdb;

import com.fasterxml.jackson.databind.JsonNode;
import okhttp3.OkHttpClient;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.*;
import org.influxdb.dto.Point.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import sk.timak.plantio.impl.RealTimeCommunicationServiceImpl;

import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Service
public class InfluxDbHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(InfluxDbHandler.class);

    private InfluxDB influxDB;
    private static final String URL_TO_INFLUXDB = "http://influxdb:8086";
    private static final String USERNAME_TO_INFLUXDB = "root";
    private static final String PASSWORD_TO_INFLUXDB = "2Dvzmtg6.";
    private static final String DATABASE_NAME = "plantioDB";
    private static final String DATABASE_RETENTION_POLICY = "autogen";

    private void connectToInfluxDb() {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        influxDB = InfluxDBFactory.connect(URL_TO_INFLUXDB, USERNAME_TO_INFLUXDB, PASSWORD_TO_INFLUXDB, client, InfluxDB.ResponseFormat.JSON);
        Pong response = influxDB.ping();
        if (response.getVersion().equalsIgnoreCase("unknown")) {
            LOGGER.info("Can not ping influxDB..");
        }
    }

    public Optional<QueryResult.Result> selectDataFromInfluxByRoomId(String roomId) {
        connectToInfluxDb();
        QueryResult queryResult = new QueryResult();
        try {
            influxDB.setDatabase(DATABASE_NAME);
            queryResult = influxDB.query(new Query("SELECT * FROM room_state WHERE \"room_id\" = '" + roomId + "'"));
            influxDB.close();
        } catch (Exception e) {
            LOGGER.info("Data getting from influxDB failed with exception : " + e);
        }
        return queryResult.getResults().stream().findFirst();
    }

    public Optional<QueryResult.Result> selectDataFromInfluxByPlantId(String plantId) {
        connectToInfluxDb();
        QueryResult queryResult = new QueryResult();
        try {
            influxDB.setDatabase(DATABASE_NAME);
            queryResult = influxDB.query(new Query("SELECT * FROM plant_state WHERE \"plant_id\" = '" + plantId + "'"));
            influxDB.close();
        } catch (Exception e) {
            LOGGER.info("Data getting from influxDB failed with exception : " + e);
        }
        return queryResult.getResults().stream().findFirst();
    }
    public void insertDataToInfluxDatabase(JsonNode json) {
        connectToInfluxDb();
        BatchPoints batchPoints = BatchPoints
                .database(DATABASE_NAME)
                .retentionPolicy(DATABASE_RETENTION_POLICY)
                .build();

        Builder builder = Point.measurement(String.valueOf(json.get("measurement")).replace("\"", ""));

        // TODO: .time will be changed to String.valueOf(json.get("time") when time.isoformat() will be ready for test from Arduino client
        builder.time(System.currentTimeMillis(), MILLISECONDS);

        // dynamically parsing JSON object for InfluxDB fields
        Iterator<Map.Entry<String, JsonNode>> iterator = json.get("fields").fields();
        while (iterator.hasNext()) {
            Map.Entry<String, JsonNode> entry = iterator.next();
            builder.addField(entry.getKey(), String.valueOf(entry.getValue()).replace("\"", ""));
        }
        batchPoints.point(builder.build());


        try {
            influxDB.write(batchPoints);
            influxDB.close();
        } catch (Exception e) {
            LOGGER.info("Data insertion to influxDB failed with exception : " + e);
        }

        //Sending json to FE using Server Send Events
        try {
            RealTimeCommunicationServiceImpl realTimeCommunicationService = new RealTimeCommunicationServiceImpl();
            realTimeCommunicationService.updateState(json);
        } catch(Exception e){
            LOGGER.error("Sending data via SSE failed : " + e);
        }
    }
}
