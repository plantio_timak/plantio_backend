package sk.timak.plantio.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import sk.timak.plantio.conf.jwt.JWTUtils;
import sk.timak.plantio.model.authorization.UserRequest;
import sk.timak.plantio.model.authorization.UserResponse;
import sk.timak.plantio.conf.security.Security;
import sk.timak.plantio.model.entity.User;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );

    @Mapping(source = "password", target = "password", qualifiedByName = "hashPassword")
    User userRequestToUser(UserRequest userRequest);

    @Mapping(source = "password", target = "password", qualifiedByName = "hashPassword")
    @Mapping(source = "userName", target = "token", qualifiedByName = "generateTokenAfterSuccessLogin")
    @Mapping(target= "services", ignore = true)
    UserResponse userToUserResponse(User user);

    @Named("hashPassword")
    static String hashPassword(String password) {
       return Security.hash(password);
    }

    @Named("generateTokenAfterSuccessLogin")
    static String generateTokenAfterSuccessLogin(String userName) {
        return JWTUtils.generateTokenAfterSuccessLogin(userName);
    }
}
