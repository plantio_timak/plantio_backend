package sk.timak.plantio.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import sk.timak.plantio.model.entity.WateringMode;

@Mapper
public interface WateringModeMapper {

    WateringModeMapper INSTANCE = Mappers.getMapper( WateringModeMapper.class );

    sk.timak.plantio.model.plant.WateringMode wateringModeToWateringModeResponse(WateringMode wateringMode);

}
