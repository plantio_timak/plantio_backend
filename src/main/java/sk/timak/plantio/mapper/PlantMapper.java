package sk.timak.plantio.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import sk.timak.plantio.model.entity.Device;
import sk.timak.plantio.model.entity.WateringMode;
import sk.timak.plantio.model.plant.Plant;


@Mapper
public interface PlantMapper {

    PlantMapper INSTANCE = Mappers.getMapper( PlantMapper.class );

    @Mapping(source = "device.id", target = "id")
    @Mapping(source = "device.type", target = "name")
    @Mapping(source = "device.plant", target = "plant")
    Plant deviceToPlantResponse(Device device);

    @Mapping(source = "wateringMode.id", target = "wateringMode.id")
    @Mapping(source = "wateringMode.name", target = "wateringMode.name")
    @Mapping(target= "name", ignore = true)
    @Mapping(target= "id", ignore = true)
    Plant wateringModeToPlantResponse(@MappingTarget Plant plant,  WateringMode wateringMode);


}
