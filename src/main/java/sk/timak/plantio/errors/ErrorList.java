package sk.timak.plantio.errors;

import java.util.*;

public class ErrorList {
    List<Error> errors;

    public ErrorList() {
    }

    public ErrorList(Collection<Error> errors) {
        HashSet<Error> errorHashSet = new HashSet<>(errors);
        this.errors = new ArrayList<>(errorHashSet);
    }


    public ErrorList(List<Error> errors) {
        this.errors = errors;
    }

    public  ErrorList(Error ... error){
        this.errors = Arrays.asList(error);
    }

    public List<Error> getErrors(){
        return this.errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
