package sk.timak.plantio.errors;

import sk.timak.plantio.errors.custom.ErrorCode;

public class Error implements ErrorCode {

    private ErrorCode error;
    private String scope;


    public Error(ErrorCode error, String scope) {
        this.error = error;
        this.scope = scope;
    }

    public Error(ErrorCode error) {
        this.error = error;
    }

    public ErrorCode getError() {
        return error;
    }

    public String getScope() {
        return scope;
    }
}
