package sk.timak.plantio.errors;

import sk.timak.plantio.errors.custom.ErrorCode;

public enum PlantioErrorCode implements ErrorCode {

    FIELD_MISSING,
    FIELD_EMPTY,
    FIELD_INVALID,
    FIELD_INVALID_TOO_FAR_IN_FUTURE,
    ID_NOT_FOUND,
    TOKEN_INVALID,
    DEVICE_ALREADY_ASSIGNED,
    ROOM_NOT_EXIST_FOR_USER,
    INCORRECT_SERVICE_INSTANCE,
    ROOM_ALREADY_EXISTS,
    FIELD_TOO_LONG

}
