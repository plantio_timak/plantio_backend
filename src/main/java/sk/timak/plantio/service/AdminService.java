package sk.timak.plantio.service;

import org.springframework.validation.Errors;
import sk.timak.plantio.model.admin.CreateRoomRequest;


public interface AdminService {

    void createRoomWithDevicesForUser(CreateRoomRequest createRoomRequest, Errors errors);

}
