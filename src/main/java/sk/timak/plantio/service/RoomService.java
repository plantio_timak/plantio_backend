package sk.timak.plantio.service;

import org.springframework.validation.Errors;
import sk.timak.plantio.model.plant.ChangeRoomInformationRequest;
import sk.timak.plantio.model.room.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface RoomService {

    List<RoomRealTimeResponse> getRoomInformation(HttpServletRequest request, String roomId, String interval, String serviceType);

    UserRoomsResponse addRoomToUser(HttpServletRequest request, AddRoomRequest addRoomRequest, Errors errors);

    UserRoomsResponse getUserRooms(HttpServletRequest request, String serviceType);

    UserRoomsResponse changeRoomInformation(HttpServletRequest request, String roomId, ChangeRoomInformationRequest changeRoomInformationRequest, String serviceType, Errors errors);
}
