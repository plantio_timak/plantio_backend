package sk.timak.plantio.service;

import org.springframework.validation.Errors;
import sk.timak.plantio.model.authorization.LoginRequest;
import sk.timak.plantio.model.authorization.UserRequest;
import sk.timak.plantio.model.authorization.UserResponse;
import sk.timak.plantio.model.entity.ServiceInstance;
import sk.timak.plantio.model.entity.User;
import sk.timak.plantio.model.entity.UserServiceInstance;
import sk.timak.plantio.model.room.LastUsedServiceInstanceRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {

    UserResponse registerOneUser(HttpServletRequest httpServletRequest, UserRequest userRequest, Errors errors);

    UserResponse loginUser(HttpServletRequest httpServletRequest, LoginRequest loginRequest, Errors errors);

    User getUserFromRequest(HttpServletRequest request);

    void setUserLastUsedServiceInstance(HttpServletRequest request, LastUsedServiceInstanceRequest serviceInstanceRequest, Errors errors);

    List<ServiceInstance> getUserServiceInstances(List<UserServiceInstance> userServiceInstances);
}
