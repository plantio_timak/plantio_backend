package sk.timak.plantio.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.context.ApplicationEventPublisher;

public interface RealTimeCommunicationService {

    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher);

    public void updateState(JsonNode jsonNode);

}
