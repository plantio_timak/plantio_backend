package sk.timak.plantio.service;

import org.springframework.validation.Errors;
import sk.timak.plantio.model.entity.WateringMode;
import sk.timak.plantio.model.plant.*;
import sk.timak.plantio.model.entity.Device;


import javax.servlet.http.HttpServletRequest;

public interface PlantService {


    PlantsResponse getUserPlants(HttpServletRequest request, String roomId, String interval);

    WateringModeResponse getWateringModes();

    Plant changePlantModeForDevice(HttpServletRequest request, ChangePlantModeRequest changePlantModeRequest, String plantId, Errors errors);

    Device getUserDeviceById(HttpServletRequest request, Long deviceId);

    WateringMode getWateringModeOfDeviceById(Long deviceId);

    Plant changePlantInformation(HttpServletRequest request, String plantId, ChangePlantInformationRequest changePlantInformationRequest, Errors errors);
}
