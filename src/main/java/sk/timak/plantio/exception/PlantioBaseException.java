package sk.timak.plantio.exception;

import org.springframework.http.HttpStatus;
import sk.timak.plantio.errors.Error;
import sk.timak.plantio.errors.PlantioErrorCode;

public class PlantioBaseException extends RuntimeException {

    public final static HttpStatus DEFAULT_HTTP_ERROR_STATUS = HttpStatus.BAD_REQUEST;

    private HttpStatus httpStatus;

    private Error error;

    private String scope;

    protected PlantioBaseException(PlantioErrorCode errorCode) {
        this(errorCode, null, DEFAULT_HTTP_ERROR_STATUS);
    }

    protected PlantioBaseException(PlantioErrorCode errorCode, String scope) {
        this(errorCode, scope, DEFAULT_HTTP_ERROR_STATUS);
    }

    protected PlantioBaseException(PlantioErrorCode errorCode, String scope, HttpStatus httpStatus) {
        this.scope = scope;
        this.error = new Error(errorCode, scope);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus(){
        return this.httpStatus;
    }

    public Error getError(){
        return this.error;
    }
}

