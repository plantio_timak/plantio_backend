package sk.timak.plantio.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import sk.timak.plantio.errors.Error;
import sk.timak.plantio.errors.ErrorList;
import sk.timak.plantio.exception.FieldTooLongException;

import java.util.Collections;

import static sk.timak.plantio.errors.PlantioErrorCode.FIELD_TOO_LONG;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        ErrorList errors = new ErrorList();
        ex.getBindingResult().getFieldErrors().stream()
                .map(error -> new Error(FIELD_TOO_LONG, error.getField()))
                .map(Collections::singletonList)
                .forEach(errors::setErrors);

        return handleExceptionInternal(new FieldTooLongException(), errors, headers, status, request);
    }
}
