package sk.timak.plantio.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import sk.timak.plantio.errors.ErrorList;
import sk.timak.plantio.exception.MultipleExceptions;
import sk.timak.plantio.exception.PlantioBaseException;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@ControllerAdvice
public class PlantioExceptionHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(PlantioExceptionHandler.class);

    @ExceptionHandler(PlantioBaseException.class)
    public ResponseEntity<ErrorList> handleException(PlantioBaseException e) {
        LOGGER.error(e.getMessage(), e);
        return new ResponseEntity<>(new ErrorList(e.getError()), createDefaultHeaders(), e.getHttpStatus());
    }

    @ExceptionHandler(MultipleExceptions.class)
    public ResponseEntity<ErrorList> handleMultipleException(MultipleExceptions e) {
        LOGGER.error("General error handling", e);
        return new ResponseEntity<>(new ErrorList(e.errors.getErrors()), createDefaultHeaders(), e.httpStatus);
    }

    private HttpHeaders createDefaultHeaders() {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(CONTENT_TYPE, APPLICATION_JSON_VALUE);
        return responseHeaders;
    }
}
