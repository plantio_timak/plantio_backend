package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class RoomNotExistException extends PlantioBaseException{

    public RoomNotExistException(String scope) {
        super(PlantioErrorCode.ROOM_NOT_EXIST_FOR_USER, scope);
    }
}