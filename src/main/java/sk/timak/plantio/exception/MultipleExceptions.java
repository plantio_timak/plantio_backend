package sk.timak.plantio.exception;

import org.springframework.http.HttpStatus;
import sk.timak.plantio.errors.Error;
import sk.timak.plantio.errors.ErrorList;

import java.util.Collection;

public class MultipleExceptions extends RuntimeException{

    public ErrorList errors;
    public HttpStatus httpStatus;

    public MultipleExceptions(Collection<Error> errors){
        this(new ErrorList(errors), PlantioBaseException.DEFAULT_HTTP_ERROR_STATUS);
    }

    public MultipleExceptions(ErrorList errorList, HttpStatus httpStatus) {
        this.errors = errorList;
        this.httpStatus = httpStatus;
    }
}
