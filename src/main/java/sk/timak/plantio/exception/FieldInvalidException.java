package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class FieldInvalidException extends PlantioBaseException{

    public FieldInvalidException(String scope) {
        super(PlantioErrorCode.FIELD_INVALID, scope);
    }
}

