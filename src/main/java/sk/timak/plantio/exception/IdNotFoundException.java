package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class IdNotFoundException extends PlantioBaseException{

    public IdNotFoundException(String scope) {
        super(PlantioErrorCode.ID_NOT_FOUND, scope);
    }
}
