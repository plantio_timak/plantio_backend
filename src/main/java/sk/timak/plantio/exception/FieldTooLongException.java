package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class FieldTooLongException extends PlantioBaseException {

    public FieldTooLongException(String scope) {
        super(PlantioErrorCode.FIELD_TOO_LONG, scope);
    }

    public FieldTooLongException() {
        super(PlantioErrorCode.FIELD_TOO_LONG);
    }
}

