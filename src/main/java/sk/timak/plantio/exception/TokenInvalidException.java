package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class TokenInvalidException extends PlantioBaseException{

    public TokenInvalidException(String scope) {
        super(PlantioErrorCode.TOKEN_INVALID, scope);
    }
}
