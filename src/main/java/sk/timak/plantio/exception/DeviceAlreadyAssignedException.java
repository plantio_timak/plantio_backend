package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class DeviceAlreadyAssignedException extends PlantioBaseException{

    public DeviceAlreadyAssignedException(String scope) {
        super(PlantioErrorCode.DEVICE_ALREADY_ASSIGNED, scope);
    }
}

