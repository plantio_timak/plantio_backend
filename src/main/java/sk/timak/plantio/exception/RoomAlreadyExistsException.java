package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class RoomAlreadyExistsException extends PlantioBaseException{

    public RoomAlreadyExistsException(String scope) {
        super(PlantioErrorCode.ROOM_ALREADY_EXISTS, scope);
    }
}
