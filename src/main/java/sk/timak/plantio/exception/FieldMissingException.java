package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class FieldMissingException extends PlantioBaseException {


    public FieldMissingException(String scope) {
        super(PlantioErrorCode.FIELD_MISSING, scope);
    }

}

