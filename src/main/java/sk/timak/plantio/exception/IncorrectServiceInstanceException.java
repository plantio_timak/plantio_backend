package sk.timak.plantio.exception;

import sk.timak.plantio.errors.PlantioErrorCode;

public class IncorrectServiceInstanceException extends PlantioBaseException{

    public IncorrectServiceInstanceException(String scope) {
        super(PlantioErrorCode.INCORRECT_SERVICE_INSTANCE, scope);
    }
}
