package sk.timak.plantio.persistance.respository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sk.timak.plantio.model.entity.WateringMode;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface WateringModeRepository extends JpaRepository<WateringMode,Long> {

    @Query(value = "select * from watering_mode", nativeQuery = true)
    List<WateringMode> getAllWateringModes();

}
