package sk.timak.plantio.persistance.respository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sk.timak.plantio.model.entity.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ServiceRepository extends JpaRepository<Service,Long> {

    @Query(value = "select * from service", nativeQuery = true)
    List<Service> getAllServices();
}
