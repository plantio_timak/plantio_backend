package sk.timak.plantio.persistance.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import sk.timak.plantio.model.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    @Query(value = "select * from user", nativeQuery = true)
    List<User> getAllUsers();

    @Modifying
    @Query(value = "insert into user(username,password,email,last_used_service_id,last_used_service_instance_id) values (:username, :password, :email, 0, 0)", nativeQuery = true)
    void insertUser(@Param("username") String userName, @Param("password") String password, @Param("email") String email);

    @Query(value = "select * from user where username = :username", nativeQuery = true)
    User getOneUser(@Param("username") String userName);
}
