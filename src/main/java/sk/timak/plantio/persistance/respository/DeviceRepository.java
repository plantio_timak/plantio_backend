package sk.timak.plantio.persistance.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.timak.plantio.model.entity.Device;

import javax.transaction.Transactional;
import java.util.List;


@Transactional
@Repository
public interface DeviceRepository extends JpaRepository<Device,Long> {

    @Query(value = "select * from device", nativeQuery = true)
    List<Device> getExistingDevices();

//    pri vytvoreni budu niektore parametre natvrdo, potom si to user moze menit
    @Modifying
    @Query(value = "insert into device(id, type, plant, use_interactive_mode, use_time_mode, time_mode_hours, watering_mode_id, service_instance_id) values (:id, :type, 'rastlina', true, false, 0, 1, :service_instance_id)", nativeQuery = true)
    void insertNewDevice(@Param("id") long id, @Param("type") String type, @Param("service_instance_id") long service_instance_id);

    @Modifying
    @Query(value = "delete from device where id = :id", nativeQuery = true)
    void deleteDevice(@Param("id") long id);
}
