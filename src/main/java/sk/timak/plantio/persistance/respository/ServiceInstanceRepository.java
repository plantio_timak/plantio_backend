package sk.timak.plantio.persistance.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.timak.plantio.model.entity.ServiceInstance;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ServiceInstanceRepository extends JpaRepository<ServiceInstance,Long> {
    @Query(value = "select * from service_instance", nativeQuery = true)
    List<ServiceInstance> getAllServiceInstances();

    @Modifying
    @Query(value = "insert into service_instance(id, ip, service_id, name) values (:id, :ip, :serviceId, :name) ", nativeQuery = true)
    void createRoomForDevices(@Param("id") long id, @Param("ip") String ip, @Param("serviceId") long serviceId, @Param("name") String name);


    @Modifying
    @Query(value = "delete from service_instance where id = :id ", nativeQuery = true)
    void deleteRoomById(@Param("id") long id);

}
