package sk.timak.plantio.persistance.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sk.timak.plantio.model.entity.UserServiceInstance;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface UserServiceInstanceRepository extends JpaRepository<UserServiceInstance,Long> {

    @Query(value = "select * from user_service_instance where user_id = :userId", nativeQuery = true)
    List<UserServiceInstance> getUserServiceInstancesById(@Param("userId") Long userId);

    @Query(value = "select service_instance_id from user_service_instance where user_id = :id", nativeQuery = true)
    List<Long> getUserRooms(@Param("id") long id);

    @Modifying
    @Query(value = "insert into user_service_instance(user_id, service_instance_id) values (:userId, :serviceInstanceId)", nativeQuery = true)
    void addRoomToUser(@Param("userId") long userId, @Param("serviceInstanceId") long serviceInstanceId);
}
