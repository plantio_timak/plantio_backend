package sk.timak.plantio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(scanBasePackages = {"sk.timak.plantio"}, exclude = SecurityAutoConfiguration.class)
public class PlantioApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlantioApplication.class, args);
    }

}
