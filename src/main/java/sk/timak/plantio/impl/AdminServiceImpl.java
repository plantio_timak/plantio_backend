package sk.timak.plantio.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import sk.timak.plantio.exception.*;
import sk.timak.plantio.model.admin.CreateRoomRequest;
import sk.timak.plantio.model.entity.ServiceInstance;
import sk.timak.plantio.model.room.Device;
import sk.timak.plantio.persistance.respository.DeviceRepository;
import sk.timak.plantio.persistance.respository.ServiceInstanceRepository;
import sk.timak.plantio.persistance.respository.ServiceRepository;
import sk.timak.plantio.service.AdminService;
import sk.timak.plantio.validators.ValidationUtils;

@Service
public class AdminServiceImpl implements AdminService {

    private Logger LOGGER = LoggerFactory.getLogger(AdminServiceImpl.class);

    private final ServiceInstanceRepository serviceInstanceRepository;

    private final ServiceRepository serviceRepository;

    private final DeviceRepository deviceRepository;


    public AdminServiceImpl(ServiceInstanceRepository serviceInstanceRepository, ServiceRepository serviceRepository, DeviceRepository deviceRepository) {
        this.serviceInstanceRepository = serviceInstanceRepository;
        this.serviceRepository = serviceRepository;
        this.deviceRepository = deviceRepository;
    }

    @Override
    public void createRoomWithDevicesForUser(CreateRoomRequest createRoomRequest, Errors errors) {
        ValidationUtils.throwPossibleValidationErrors(errors);

        sk.timak.plantio.model.entity.Service service = serviceRepository.getAllServices().stream()
                .filter(service1 -> service1.getName().toString().equals(createRoomRequest.getServiceType()))
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("serviceType"));

        try {
            serviceInstanceRepository.createRoomForDevices(createRoomRequest.getId(), createRoomRequest.getIp(), service.getId(), createRoomRequest.getName());
        }
        catch (Exception e){
            throw new RoomAlreadyExistsException(createRoomRequest.getName());
        }

        ServiceInstance serviceInstance = serviceInstanceRepository.getAllServiceInstances().stream()
                .filter(serviceInstance1 -> serviceInstance1.getId() == createRoomRequest.getId())
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("id"));

        createRoomRequest.getDevices().forEach(device -> {
            try {
                deviceRepository.insertNewDevice(device.getId(), device.getType(), createRoomRequest.getId());
                LOGGER.info("admin inserting new device {}", device.getId());
            }
            catch (Exception e) {
                for (Device requestedDevice : createRoomRequest.getDevices()) {
                    if (device.getId() != requestedDevice.getId()) {
                        deviceRepository.deleteDevice(requestedDevice.getId());
                    } else break;
                }
                serviceInstanceRepository.deleteRoomById(serviceInstance.getId());
                throw new DeviceAlreadyAssignedException(device.getType());
            }
        });
    }

}
