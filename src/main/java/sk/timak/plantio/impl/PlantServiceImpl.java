package sk.timak.plantio.impl;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.influxdb.InfluxDBException;
import org.influxdb.dto.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import sk.timak.plantio.conf.mqtt.MqttConnection;
import sk.timak.plantio.conf.mqtt.MqttConstants;
import sk.timak.plantio.exception.FieldInvalidException;
import sk.timak.plantio.exception.FieldMissingException;
import sk.timak.plantio.exception.IdNotFoundException;
import sk.timak.plantio.exception.RoomNotExistException;
import sk.timak.plantio.influxdb.InfluxDbHandler;
import sk.timak.plantio.mapper.PlantMapper;
import sk.timak.plantio.mapper.WateringModeMapper;
import sk.timak.plantio.model.entity.Device;
import sk.timak.plantio.model.entity.User;
import sk.timak.plantio.model.entity.WateringMode;
import sk.timak.plantio.model.plant.*;
import sk.timak.plantio.model.room.GraphInterval;
import sk.timak.plantio.persistance.respository.DeviceRepository;
import sk.timak.plantio.persistance.respository.UserServiceInstanceRepository;
import sk.timak.plantio.persistance.respository.WateringModeRepository;
import sk.timak.plantio.service.PlantService;
import sk.timak.plantio.service.UserService;
import sk.timak.plantio.validators.ValidationUtils;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static sk.timak.plantio.model.plant.PlantMode.INTERACTIVE;
import static sk.timak.plantio.model.plant.PlantMode.TIME;

@Service
public class PlantServiceImpl extends ServiceUtils implements PlantService {

    private final Logger LOGGER = LoggerFactory.getLogger(PlantServiceImpl.class);

    private final DeviceRepository deviceRepository;

    private final UserServiceInstanceRepository userServiceInstanceRepository;

    private final WateringModeRepository wateringModeRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private final InfluxDbHandler influxDbHandler;

    @Autowired
    private final MqttConnection mqttConnection;

    public PlantServiceImpl(DeviceRepository deviceRepository, UserServiceInstanceRepository userServiceInstanceRepository,
                            WateringModeRepository wateringModeRepository, InfluxDbHandler influxDbHandler, MqttConnection mqttConnection) {
        this.deviceRepository = deviceRepository;
        this.userServiceInstanceRepository = userServiceInstanceRepository;
        this.wateringModeRepository = wateringModeRepository;
        this.influxDbHandler = influxDbHandler;
        this.mqttConnection = mqttConnection;
    }

    public List<PlantRealTimeResponse> getPlantInformation(String plantId, String interval) {
        if ("".equals(interval)) {
            interval = GraphInterval.DAY.toString();
        }
        GraphInterval graphInterval = mapGraphInterval(interval);
        List<PlantRealTimeResponse> realTimeResponseData =  mapPlantRealTimeData(plantId).stream()
                .filter(timeValue -> timeValue.getTime().isAfter(filterTimeFromResponseByGraphInterval(graphInterval)))
                .sorted(Comparator.comparing(PlantRealTimeResponse::getTime).reversed())
                .collect(Collectors.toList());
        return reduceDataByGraphInterval(realTimeResponseData, graphInterval);
    }

    private List<PlantRealTimeResponse> mapPlantRealTimeData(String plantId) {
        Optional<QueryResult.Result> plantInformationResult = influxDbHandler.selectDataFromInfluxByPlantId(plantId);
        List<PlantRealTimeResponse> response = new ArrayList<>();
        plantInformationResult.ifPresent(series -> {
            if (series.getError() != null) {
                throw new InfluxDBException(series.getError());
            } else if (series.getSeries() != null && series.getSeries().stream().findFirst().isPresent()) {
                checkPlantSeriesColumns(series.getSeries().stream().findFirst().get().getColumns());
                series.getSeries().stream().findFirst().get().getValues().forEach(value -> {
                    PlantRealTimeResponse realTimeResponse = new PlantRealTimeResponse();
                    setTimeToRealTimeResponse(realTimeResponse, String.valueOf(value.get(0)));
                    PlantValues plantValues = new PlantValues(Double.parseDouble(value.get(1).toString()), Double.parseDouble(value.get(3).toString()));
                    realTimeResponse.setValues(plantValues);
                    response.add(realTimeResponse);
                });
            }
        });
        return response;
    }

    private void checkPlantSeriesColumns(List<String> columns) {
        if (columns != null && columns.size() == 4) {
            if (!"time".equals(columns.get(0)) || !"humidity".equals(columns.get(1)) ||
                    !"plant_id".equals(columns.get(2)) || !"temperature".equals(columns.get(3)))
                throw new InfluxDBException("wrong columns order");
        }
    }

    @Override
    public PlantsResponse getUserPlants(HttpServletRequest request, String roomId, String interval) {

        User user = userService.getUserFromRequest(request);

        List<Long> rooms = userServiceInstanceRepository.getUserRooms(user.getId());

        if (rooms.contains(Long.parseLong(roomId))) {
            PlantsResponse plantsResponse = new PlantsResponse();

            List<Device> devices = deviceRepository.getExistingDevices().stream()
                    .filter(device -> device.getServiceInstanceId() == Long.parseLong(roomId))
                    .collect(Collectors.toList());

            plantsResponse.setPlants(devices.stream()
                    .map(device -> mapPlant(device, interval))
                    .collect(Collectors.toList()));

            LOGGER.info("GET user plants");

            return plantsResponse;
        } else throw new RoomNotExistException(roomId);
    }

    private Plant mapPlant(Device device, String interval) {
        WateringMode wateringMode = getWateringModeOfDeviceById(device.getWateringModeId());
        Plant plant = PlantMapper.INSTANCE.deviceToPlantResponse(device);
        plant.setMode(mapMode(device));
        if (Objects.equals(plant.getMode().getPlantMode(), PlantMode.INTERACTIVE)) {
            plant = PlantMapper.INSTANCE.wateringModeToPlantResponse(plant, wateringMode);
        }
        plant.setPlantValues(this.getPlantInformation(String.valueOf(device.getId()), interval));
        return plant;
    }

    private Mode mapMode(Device device) {
        Mode mode = new Mode();
        if (device.isUseInteractiveMode()) {
            mode.setPlantMode(PlantMode.INTERACTIVE);
        } else if (device.isUseTimeMode()) {
            mode.setPlantMode(TIME);
            mode.setTime(String.valueOf(device.getTimeModeHours()));
        }
        return mode;
    }

    @Override
    public WateringModeResponse getWateringModes() {

        LOGGER.info("GET watering modes");

        return new WateringModeResponse(wateringModeRepository.getAllWateringModes().stream()
                .map(WateringModeMapper.INSTANCE::wateringModeToWateringModeResponse)
                .collect(Collectors.toList()));
    }

    private Device changeWateringModeForDevice(Device device, long wateringModeId) {
        if (isWateringModeChangeable(device, wateringModeId) && device.isUseInteractiveMode()) {
            device.setWateringModeId(wateringModeId);
            LOGGER.info("PUT watering mode for plant {}", device.getId());
        }
        return device;
    }

    private boolean isWateringModeChangeable(Device device, long wateringModeId) {
        return (device.getWateringModeId() != wateringModeId &&
                this.getWateringModes().getWateringModes().stream()
                        .map(sk.timak.plantio.model.plant.WateringMode::getId)
                        .collect(Collectors.toList())
                        .contains(wateringModeId));
    }

    private boolean isRequestChangeable(Device device, ChangePlantModeRequest changePlantModeRequest) {
        return (isWateringModeChangeable(device, changePlantModeRequest.getWateringModeId()) ||
                isPlantModeChangeable(device, changePlantModeRequest.getPlantMode()));
    }

    private boolean isPlantModeChangeable(Device device, String plantMode) {
        return ((device.isUseInteractiveMode() && TIME.toString().equals(plantMode)) ||
                (device.isUseTimeMode() && INTERACTIVE.toString().equals(plantMode)));
    }

    @Override
    public Plant changePlantModeForDevice(HttpServletRequest request, ChangePlantModeRequest changePlantModeRequest, String plantId, Errors errors) {

        ValidationUtils.throwPossibleValidationErrors(errors);
        Device device = this.getUserDeviceById(request, Long.parseLong(plantId));

        if (isRequestChangeable(device, changePlantModeRequest)) {
            if (changePlantModeRequest.getPlantMode().equals(PlantMode.INTERACTIVE.name())) {
                device.setUseInteractiveMode(true);
                device.setUseTimeMode(false);
                device = changeWateringModeForDevice(device, changePlantModeRequest.getWateringModeId());
                deviceRepository.save(device);
            } else if (changePlantModeRequest.getPlantMode().equals(TIME.name())) {
                if (changePlantModeRequest.getTime() <= 0) {
                    throw new FieldMissingException("time");
                }
                device.setUseTimeMode(true);
                device.setUseInteractiveMode(false);
                device.setTimeModeHours(changePlantModeRequest.getTime());
                deviceRepository.save(device);
            }

            MqttMessage message = mqttConnection.createMqttMessage(device,changePlantModeRequest.getPlantMode());
            mqttConnection.publish(MqttConstants.WATER_MODE_TOPIC,message);

            LOGGER.info("PUT plant mode for plant {}", device.getId());
            return mapPlant(device, GraphInterval.DAY.toString());
        }
        return null;
    }

    @Override
    public Device getUserDeviceById(HttpServletRequest request, Long plantId) {

        Device device = deviceRepository.getExistingDevices().stream()
                .filter(device1 -> device1.getId() == plantId)
                .findFirst()
                .orElseThrow(() -> new IdNotFoundException("plantId"));

        User user = userService.getUserFromRequest(request);

        userServiceInstanceRepository.getUserServiceInstancesById(user.getId()).stream()
                .filter(userServiceInstance1 -> userServiceInstance1.getServiceInstanceId() == device.getServiceInstanceId())
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("plantId"));

        LOGGER.info("GET user device {}", device.getId());

        return device;
    }

    @Override
    public WateringMode getWateringModeOfDeviceById(Long wateringModeId) {
        return wateringModeRepository.getAllWateringModes().stream()
                .filter(wateringMode -> wateringMode.getId() == wateringModeId)
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("wateringModeId"));
    }

    @Override
    public Plant changePlantInformation(HttpServletRequest request, String plantId, ChangePlantInformationRequest changePlantInformationRequest, Errors errors) {
        ValidationUtils.throwPossibleValidationErrors(errors);
        Device device = this.getUserDeviceById(request, Long.parseLong(plantId));
        boolean isDeviceChanged = false;
        if (changePlantInformationRequest.getName() != null) {
            if (!Objects.equals(device.getType(), changePlantInformationRequest.getName())) {
                device.setType(changePlantInformationRequest.getName());
                isDeviceChanged = true;
            }
        }
        if (changePlantInformationRequest.getPlant() != null) {
            if (!Objects.equals(device.getPlant(), changePlantInformationRequest.getPlant())) {
                device.setPlant(changePlantInformationRequest.getPlant());
                isDeviceChanged = true;
            }
        }
        if (isDeviceChanged) {
            deviceRepository.save(device);
            LOGGER.info("PUT plant name for plant {}", device.getId());
            return mapPlant(device, GraphInterval.DAY.toString());
        } else return null;

    }

    private List<PlantRealTimeResponse> reduceDataByGraphInterval(List<PlantRealTimeResponse> realTimeResponses, GraphInterval graphInterval) {
        if (GraphInterval.DAY.equals(graphInterval)) {
            return realTimeResponses;
        }
        //       get average every 12h
        else if (GraphInterval.WEEK.equals(graphInterval)) {
            ZonedDateTime now = ZonedDateTime.now();
            List<List<PlantRealTimeResponse>> helpResponses = new ArrayList<>();
            for (int i = 0; i < 14; i++) {
                now = now.minusHours(12);
                List<PlantRealTimeResponse> plantRealTimeResponses = new ArrayList<>();
                for (PlantRealTimeResponse timeValue : realTimeResponses) {
                    if (timeValue.getTime().isAfter(now)) {
                        if (timeValue.getTime().isBefore(now.plusHours(12))) {
                            plantRealTimeResponses.add(timeValue);
                        }
                    }
                }
                helpResponses.add(plantRealTimeResponses);
            }
            return averageValuesOfRealTimeResponses(helpResponses);

            //       get average every day
        } else if (GraphInterval.MONTH.equals(graphInterval)) {
            List<List<PlantRealTimeResponse>> helpResponses = new ArrayList<>();
            ZonedDateTime now = ZonedDateTime.now();
            for (int i = 0; i < 31; i++) {
                now = now.minusDays(1);
                List<PlantRealTimeResponse> plantRealTimeResponses = new ArrayList<>();
                for (PlantRealTimeResponse response : realTimeResponses) {
                    if (response.getTime().isAfter(now)) {
                        if (response.getTime().isBefore(now.plusDays(1))) {
                            plantRealTimeResponses.add(response);
                        }
                    }
                }
                helpResponses.add(plantRealTimeResponses);
            }
            return averageValuesOfRealTimeResponses(helpResponses);

        } else throw new FieldInvalidException("interval");

    }

    private List<PlantRealTimeResponse> averageValuesOfRealTimeResponses(List<List<PlantRealTimeResponse>> helpResponses){
        List<PlantRealTimeResponse> result = new ArrayList<>();
        for (List<PlantRealTimeResponse> oneLimitedResponse : helpResponses) {
            PlantRealTimeResponse finalResponse = new PlantRealTimeResponse();

            double mediumOfHumidity = oneLimitedResponse.stream()
                    .map(PlantRealTimeResponse::getValues)
                    .map(PlantValues::getHumidity)
                    .mapToDouble(Double::doubleValue).sum() / (double) oneLimitedResponse.size();

            double mediumOfTemperature = oneLimitedResponse.stream()
                    .map(PlantRealTimeResponse::getValues)
                    .map(PlantValues::getTemperature)
                    .mapToDouble(Double::doubleValue).sum() / (double) oneLimitedResponse.size();
            Optional<PlantRealTimeResponse> realTimeResponseOptional = oneLimitedResponse.stream().findFirst();
            if (realTimeResponseOptional.isPresent()) {
                finalResponse.setTime(realTimeResponseOptional.get().getTime());
                finalResponse.setValues(new PlantValues(Math.round(mediumOfHumidity * 100.0)/100.0, Math.round(mediumOfTemperature * 100.0)/100.0));
                result.add(finalResponse);
            }
        }
        return result;
    }
}
