package sk.timak.plantio.impl;

import org.influxdb.InfluxDBException;
import org.influxdb.dto.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import sk.timak.plantio.exception.DeviceAlreadyAssignedException;
import sk.timak.plantio.exception.FieldInvalidException;
import sk.timak.plantio.exception.IdNotFoundException;
import sk.timak.plantio.influxdb.InfluxDbHandler;
import sk.timak.plantio.model.entity.ServiceInstance;
import sk.timak.plantio.model.entity.User;
import sk.timak.plantio.model.entity.UserServiceInstance;
import sk.timak.plantio.model.plant.ChangeRoomInformationRequest;
import sk.timak.plantio.model.room.*;
import sk.timak.plantio.persistance.respository.ServiceInstanceRepository;
import sk.timak.plantio.persistance.respository.ServiceRepository;
import sk.timak.plantio.persistance.respository.UserRepository;
import sk.timak.plantio.persistance.respository.UserServiceInstanceRepository;
import sk.timak.plantio.service.RoomService;
import sk.timak.plantio.service.UserService;
import sk.timak.plantio.validators.ValidationUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class RoomServiceImpl extends ServiceUtils implements RoomService {

    private final Logger LOGGER = LoggerFactory.getLogger(RoomServiceImpl.class);

    private final ServiceInstanceRepository serviceInstanceRepository;

    private final UserServiceInstanceRepository userServiceInstanceRepository;

    private final UserRepository userRepository;

    private final ServiceRepository serviceRepository;

    @Autowired
    private final InfluxDbHandler influxDbHandler;

    @Autowired
    private UserService userService;

    public RoomServiceImpl(ServiceInstanceRepository serviceInstanceRepository, UserServiceInstanceRepository userServiceInstanceRepository,
                           UserRepository userRepository, ServiceRepository serviceRepository, InfluxDbHandler influxDbHandler) {
        this.serviceInstanceRepository = serviceInstanceRepository;
        this.userServiceInstanceRepository = userServiceInstanceRepository;
        this.userRepository = userRepository;
        this.serviceRepository = serviceRepository;
        this.influxDbHandler = influxDbHandler;
    }

    @Override
    public List<RoomRealTimeResponse> getRoomInformation(HttpServletRequest request, String roomId, String interval, String serviceType) {
        if ("".equals(interval)) {
            interval = GraphInterval.DAY.toString();
        }
        this.getUserServiceInstancesByServiceType(request, serviceType).stream()
                .filter(serviceInstance1 -> serviceInstance1.getId() == Long.parseLong(roomId))
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("roomId"));

        GraphInterval graphInterval = mapGraphInterval(interval);

        return mapRoomRealTimeData(roomId).stream()
                .filter(timeValue -> timeValue.getTime().isAfter(filterTimeFromResponseByGraphInterval(graphInterval)))
                .sorted(Comparator.comparing(RoomRealTimeResponse::getTime).reversed())
                .collect(Collectors.toList());
    }

    private List<RoomRealTimeResponse> mapRoomRealTimeData(String roomId) {
        Optional<QueryResult.Result> roomInformationResult = influxDbHandler.selectDataFromInfluxByRoomId(roomId);
        List<RoomRealTimeResponse> response = new ArrayList<>();

        roomInformationResult.ifPresent(series -> {
            if (series.getError() != null) {
                throw new InfluxDBException(series.getError());
            } else if (series.getSeries() != null && series.getSeries().stream().findFirst().isPresent()) {
                checkRoomSeriesColumns(series.getSeries().stream().findFirst().get().getColumns());
                series.getSeries().stream().findFirst().get().getValues().forEach(value -> {
                    RoomRealTimeResponse realTimeResponse = new RoomRealTimeResponse();
                    RoomValues roomValues = new RoomValues();
                    boolean result = mapRoomValues(roomValues, value);
                    if (result) {
                        realTimeResponse.setValues(roomValues);
                        setTimeToRealTimeResponse(realTimeResponse, String.valueOf(value.get(0)));
                        response.add(realTimeResponse);
                    }
                });
            }
        });
        return response;
    }

    private boolean mapRoomValues(RoomValues roomValues, List<Object> value) {
        try {
            roomValues.setHumidity(Double.parseDouble(value.get(1).toString()));
            roomValues.setLightIntensity(Double.parseDouble(value.get(2).toString()));
            roomValues.setTemperature(Double.parseDouble(value.get(4).toString()));
            mapWaterCapacity(roomValues, value);
            return true;
        } catch (Exception e) {
            LOGGER.error("invalid format of room value");
            return false;
        }
    }

    private void mapWaterCapacity(RoomValues roomValues, List<Object> value) {
//        ugly hack, only because not all data contains water level
        Optional<Object> waterCapacity = Optional.ofNullable(value.get(5));
        if (waterCapacity.isPresent()) {
            if (waterCapacity.get().toString().equals("false")) {
                roomValues.setWaterCapacity(false);
            } else if (waterCapacity.get().toString().equals("true")) {
                roomValues.setWaterCapacity(true);
            }
        }
    }

    private void checkRoomSeriesColumns(List<String> columns) {
        if (columns != null && (columns.size() == 5 || columns.size() == 6)) {
            if (!"time".equals(columns.get(0)) || !"humidity".equals(columns.get(1)) ||
                    !"light_intensity".equals(columns.get(2)) || !"room_id".equals(columns.get(3)) ||
                    !"temperature".equals(columns.get(4)))
                throw new InfluxDBException("wrong colmuns order");
        }
    }

    @Override
    public UserRoomsResponse addRoomToUser(HttpServletRequest request, AddRoomRequest addRoomRequest, Errors errors) {
        ValidationUtils.throwPossibleValidationErrors(errors);
        User user = userService.getUserFromRequest(request);

        ServiceInstance serviceInstance = serviceInstanceRepository.getAllServiceInstances()
                .stream()
                .filter(item -> addRoomRequest.getDeviceId() == item.getId())
                .findFirst()
                .orElseThrow(() -> new IdNotFoundException("deviceId"));

        Optional<UserServiceInstance> userServiceInstance = userServiceInstanceRepository.getUserServiceInstancesById(user.getId())
                .stream()
                .filter(instance -> (instance.getServiceInstanceId() == serviceInstance.getId()))
                .findFirst();

        if (userServiceInstance.isEmpty()) {
            userServiceInstanceRepository.addRoomToUser(user.getId(), serviceInstance.getId());
            user.setLastUsedServiceId(getServiceByName(addRoomRequest.getServiceType()).getId());
            userRepository.save(user);
            LOGGER.info("adding new user room {}", serviceInstance.getId());
            return getUserRooms(request, addRoomRequest.getServiceType());
        } else {
            throw new DeviceAlreadyAssignedException("device");
        }
    }

    @Override
    public UserRoomsResponse getUserRooms(HttpServletRequest request, String serviceType) {

        List<ServiceInstance> serviceInstances = userService.getUserServiceInstances(getUserServiceInstancesFromRequest(request)).stream()
                .filter(serviceInstance -> getServiceByName(serviceType).getId() == serviceInstance.getServiceId())
                .collect(Collectors.toList());

        UserRoomsResponse response = new UserRoomsResponse();

        response.setUserRooms(serviceInstances.stream()
                .map(serviceInstance -> new UserRoom(serviceInstance.getId(), serviceInstance.getName()))
                .sorted(Comparator.comparing(UserRoom::getId))
                .collect(Collectors.toList()));

        LOGGER.info("GET user rooms");

        return response;
    }

    private sk.timak.plantio.model.entity.Service getServiceByName(String serviceName) {

        return serviceRepository.getAllServices().stream()
                .filter(service -> service.getName().toString().equals(serviceName))
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("serviceName"));
    }

    private List<ServiceInstance> getUserServiceInstancesByServiceType(HttpServletRequest request, String serviceType) {
        return userService.getUserServiceInstances(getUserServiceInstancesFromRequest(request)).stream()
                .filter(serviceInstance -> getServiceByName(serviceType).getId() == serviceInstance.getServiceId())
                .collect(Collectors.toList());
    }

    private List<UserServiceInstance> getUserServiceInstancesFromRequest(HttpServletRequest request) {
        User user = userService.getUserFromRequest(request);
        return userServiceInstanceRepository.getUserServiceInstancesById(user.getId());
    }

    @Override
    public UserRoomsResponse changeRoomInformation(HttpServletRequest request, String roomId, ChangeRoomInformationRequest changeRoomInformationRequest, String serviceType, Errors errors) {
        ValidationUtils.throwPossibleValidationErrors(errors);

        ServiceInstance serviceInstance = this.getUserServiceInstancesByServiceType(request, serviceType).stream()
                .filter(serviceInstance1 -> serviceInstance1.getId() == Long.parseLong(roomId))
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("roomId"));

        if (!Objects.equals(serviceInstance.getName(), changeRoomInformationRequest.getName())) {
            serviceInstance.setName(changeRoomInformationRequest.getName());
            serviceInstanceRepository.save(serviceInstance);
            LOGGER.info("PUT room name for room {}", serviceInstance.getId());
            return this.getUserRooms(request, serviceType);
        }
        return null;
    }
}
