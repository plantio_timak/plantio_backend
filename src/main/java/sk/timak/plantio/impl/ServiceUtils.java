package sk.timak.plantio.impl;

import sk.timak.plantio.exception.FieldInvalidException;
import sk.timak.plantio.model.RealTimeResponse;
import sk.timak.plantio.model.room.GraphInterval;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public abstract class ServiceUtils {


    protected GraphInterval mapGraphInterval(String interval) {
        if (interval.equals(GraphInterval.DAY.toString())) {
            return GraphInterval.DAY;
        } else if (interval.equals(GraphInterval.WEEK.toString())) {
            return GraphInterval.WEEK;
        } else if (interval.equals(GraphInterval.MONTH.toString())) {
            return GraphInterval.MONTH;
        } else throw new FieldInvalidException("interval");
    }

    protected void setTimeToRealTimeResponse(RealTimeResponse realTimeResponse, String time) {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
                .appendFraction(ChronoField.MILLI_OF_SECOND, 0, 3, true)
                .appendPattern("'Z'")
                .toFormatter();
        LocalDateTime localDateTime = LocalDateTime.parse(time, formatter);
        ZoneId zoneId = ZoneId.of("Europe/Paris");
        ZonedDateTime zdt = ZonedDateTime.of(localDateTime, zoneId);

        realTimeResponse.setTime(zdt);
    }

    protected ZonedDateTime filterTimeFromResponseByGraphInterval(GraphInterval graphInterval) {
        if (GraphInterval.DAY.equals(graphInterval)) {
            ZonedDateTime now = ZonedDateTime.now();
            now = now.minusDays(1);
            return now;
        } else if (GraphInterval.WEEK.equals(graphInterval)) {
            ZonedDateTime now = ZonedDateTime.now();
            now = now.minusWeeks(1);
            return now;
        } else if (GraphInterval.MONTH.equals(graphInterval)) {
            ZonedDateTime now = ZonedDateTime.now();
            now = now.minusMonths(1);
            return now;
        } else throw new FieldInvalidException("interval");
    }
}
