package sk.timak.plantio.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import sk.timak.plantio.conf.jwt.JWTUtils;
import sk.timak.plantio.exception.FieldInvalidException;
import sk.timak.plantio.interfaces.ServiceType;
import sk.timak.plantio.mapper.UserMapper;
import org.springframework.stereotype.Service;
import sk.timak.plantio.model.authorization.Flag;
import sk.timak.plantio.model.authorization.LoginRequest;
import sk.timak.plantio.model.authorization.UserRequest;
import sk.timak.plantio.model.authorization.UserResponse;
import sk.timak.plantio.model.entity.ServiceInstance;
import sk.timak.plantio.model.entity.User;
import sk.timak.plantio.model.entity.UserServiceInstance;
import sk.timak.plantio.model.room.LastUsedServiceInstanceRequest;
import sk.timak.plantio.persistance.respository.ServiceInstanceRepository;
import sk.timak.plantio.persistance.respository.ServiceRepository;
import sk.timak.plantio.persistance.respository.UserRepository;
import sk.timak.plantio.persistance.respository.UserServiceInstanceRepository;
import sk.timak.plantio.service.UserService;
import sk.timak.plantio.conf.security.Security;
import sk.timak.plantio.validators.ValidationUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

import static sk.timak.plantio.conf.SecurityConstants.ADMIN;

@Service
public class UserServiceImpl implements UserService {

    private Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    private final UserServiceInstanceRepository userServiceInstanceRepository;

    private final ServiceRepository serviceRepository;

    private final ServiceInstanceRepository serviceInstanceRepository;

    public UserServiceImpl(UserRepository userRepository, UserServiceInstanceRepository userServiceInstanceRepository,
                           ServiceInstanceRepository serviceInstanceRepository, ServiceRepository serviceRepository) {
        this.userRepository = userRepository;
        this.userServiceInstanceRepository = userServiceInstanceRepository;
        this.serviceRepository = serviceRepository;
        this.serviceInstanceRepository = serviceInstanceRepository;
    }

    @Override
    public UserResponse registerOneUser(HttpServletRequest httpServletRequest, UserRequest userRequest, Errors errors) {
        ValidationUtils.throwPossibleValidationErrors(errors);

        if (userNameExists(userRequest.getUserName()) || userNameIsBlocked(userRequest.getUserName())) {
            throw new FieldInvalidException("userName");
        }
        User mappedUser = UserMapper.INSTANCE.userRequestToUser(userRequest);
        userRepository.insertUser(mappedUser.getUserName(), mappedUser.getPassword(), mappedUser.getEmail());
        UserResponse response = UserMapper.INSTANCE.userToUserResponse(mappedUser);

        response.setServices(Collections.emptyList());

        response.setSessionId(JWTUtils.getSessionIdFromRequest(httpServletRequest));
        LOGGER.info("Successful registered user: {} with sessionId: {}", response.getUserName(), JWTUtils.getSessionIdFromRequest(httpServletRequest));

        return response;
    }

    @Override
    public UserResponse loginUser(HttpServletRequest httpServletRequest, LoginRequest loginRequest, Errors errors) {
        ValidationUtils.throwPossibleValidationErrors(errors);
        User user = userRepository.getAllUsers()
                .stream()
                .filter(u1 -> loginRequest.getUserName().equals(u1.getUserName()))
                .filter(u2 -> Security.hash(loginRequest.getPassword()).equals(u2.getPassword()))
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("userName or password"));

        List<UserServiceInstance> userServiceInstances = userServiceInstanceRepository.getUserServiceInstancesById(user.getId());

        UserResponse response = UserMapper.INSTANCE.userToUserResponse(user);
        response.setLastUsedService(mapLastUsedServiceToUserResponse(user));
        mapLastUsedServiceInstanceToUserResponse(user, userServiceInstances, response);

        response.setServices(getUserServices(userServiceInstances));
        if (JWTUtils.isRoleAdminUser(response.getToken())) {
            response.setFlags(Collections.singletonList(Flag.IS_ADMIN));
        }

        response.setSessionId(JWTUtils.getSessionIdFromRequest(httpServletRequest));
        LOGGER.info("Successful logged user: {} with sessionId: {}", response.getUserName(), JWTUtils.getSessionIdFromRequest(httpServletRequest));

        return response;
    }

    private String mapLastUsedServiceToUserResponse(User user) {

        return serviceRepository.getAllServices().stream()
                .filter(service1 -> service1.getId() == user.getLastUsedServiceId())
                .findFirst()
                .map(service -> service.getName().toString())
                .orElse(null);
    }

    private void mapLastUsedServiceInstanceToUserResponse(User user, List<UserServiceInstance> userServiceInstances, UserResponse userResponse) {

        userResponse.setLastUsedServiceInstance(getUserServiceInstances(userServiceInstances).stream()
                .map(ServiceInstance::getId)
                .filter(id -> id == user.getLastUsedServiceInstanceId())
                .findFirst()
                .orElse(0L));
    }

    @Override
    public void setUserLastUsedServiceInstance(HttpServletRequest request, LastUsedServiceInstanceRequest serviceInstanceRequest, Errors errors) {
        ValidationUtils.throwPossibleValidationErrors(errors);

        try {
            User user = getUserFromRequest(request);
            List<UserServiceInstance> userServiceInstances = userServiceInstanceRepository.getUserServiceInstancesById(user.getId());
            List<Long> serviceInstancesIds = getUserServiceInstances(userServiceInstances).stream()
                    .map(ServiceInstance::getId)
                    .collect(Collectors.toList());
            if (serviceInstancesIds.contains(serviceInstanceRequest.getLastUsedServiceInstanceId())) {
                user.setLastUsedServiceInstanceId(serviceInstanceRequest.getLastUsedServiceInstanceId());
                userRepository.save(user);
            }
            else throw new FieldInvalidException("serviceInstanceId");

        } catch (Exception e) {
            throw new FieldInvalidException("serviceInstanceId");
        }
    }

    private boolean userNameExists(String userName) {
        return userRepository.getOneUser(userName) != null;
    }

    private boolean userNameIsBlocked(String userName) {
        return userName.toLowerCase().contains(ADMIN);
    }

    private List<ServiceType> getUserServices(List<UserServiceInstance> userServiceInstances) {

        List<Long> userServiceIds = getUserServiceInstances(userServiceInstances).stream()
                .map(ServiceInstance::getServiceId).collect(Collectors.toList());

        return serviceRepository.getAllServices().stream()
                .filter(service -> userServiceIds.contains(service.getId()))
                .map(sk.timak.plantio.model.entity.Service::getName)
                .collect(Collectors.toList());
    }

    @Override
    public List<ServiceInstance> getUserServiceInstances(List<UserServiceInstance> userServiceInstances) {
        List<ServiceInstance> serviceInstances = new ArrayList<>();

        userServiceInstances.stream().map(userServiceInstance -> serviceInstanceRepository.getAllServiceInstances().stream()
                .filter(serviceInstance1 -> serviceInstance1.getId() == userServiceInstance.getServiceInstanceId())
                .findFirst())
                .forEach(serviceInstance -> serviceInstance.ifPresent(serviceInstances::add));

        return serviceInstances;
    }

    public User getUserFromRequest(HttpServletRequest request) {
        String userName = JWTUtils.getUserNameFromRequest(request);

        return userRepository.getAllUsers()
                .stream()
                .filter(u1 -> userName.equals(u1.getUserName()))
                .findFirst()
                .orElseThrow(() -> new FieldInvalidException("userName"));
    }
}
