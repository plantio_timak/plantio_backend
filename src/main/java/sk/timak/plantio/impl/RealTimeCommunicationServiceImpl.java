package sk.timak.plantio.impl;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import sk.timak.plantio.service.RealTimeCommunicationService;

@Service
public class RealTimeCommunicationServiceImpl implements RealTimeCommunicationService, ApplicationEventPublisherAware {
    private static ApplicationEventPublisher publisher;

    public RealTimeCommunicationServiceImpl(){

    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        publisher = applicationEventPublisher;
    }

    public void updateState(JsonNode jsonNode){
        publisher.publishEvent(jsonNode);
    }
}