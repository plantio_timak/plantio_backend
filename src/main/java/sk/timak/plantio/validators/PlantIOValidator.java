package sk.timak.plantio.validators;

import sk.timak.plantio.errors.PlantioErrorCode;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static sk.timak.plantio.errors.PlantioErrorCode.*;

public class PlantIOValidator implements ConstraintValidator<ValidPlantioField, Object> {


    private boolean mandatory;

    @Override
    public void initialize(ValidPlantioField constraintAnnotation) {
        this.mandatory = constraintAnnotation.mandatory();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        PlantioErrorCode errorCode = null;

        if (mandatory) {
            if (object == null) {
                errorCode = FIELD_MISSING;
            } else if (object instanceof String && object.toString().equals("")) {
                errorCode = FIELD_EMPTY;
            }
        }
        if (errorCode != null) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(errorCode.toString()).addConstraintViolation();
            return false;
        }
        return true;
    }

}
