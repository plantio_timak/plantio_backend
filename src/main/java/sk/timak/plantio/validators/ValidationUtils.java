package sk.timak.plantio.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import sk.timak.plantio.errors.Error;
import sk.timak.plantio.errors.PlantioErrorCode;
import sk.timak.plantio.exception.MultipleExceptions;

import java.util.ArrayList;
import java.util.List;

public class ValidationUtils {

    private static Logger LOGGER = LoggerFactory.getLogger(ValidationUtils.class);

    public static void throwPossibleValidationErrors(Errors errors){
        List<Error> errorList = parseValidationErrors(errors);
        if (!errorList.isEmpty()){
            errorList.forEach(error -> LOGGER.error("Error: {},  scope:{}" , error.getError(), error.getScope()));
            throw new MultipleExceptions(errorList);
        }
    }

    public static List<Error> parseValidationErrors (Errors errors) {
        List<Error> errorList = new ArrayList<>();

        if (errors.hasErrors()) {
            String scope;

            for (FieldError fieldError : errors.getFieldErrors()) {

                scope = fieldError.getField();

                if (fieldError.getRejectedValue() == null && fieldError.getDefaultMessage().equals("FIELD_INVALID"))
                    errorList.add(new Error(PlantioErrorCode.FIELD_MISSING, scope));
                else
                    errorList.add(new Error(PlantioErrorCode.valueOf(fieldError.getDefaultMessage()), scope));
            }

        }
        return errorList;
    }
}
