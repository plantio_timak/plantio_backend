package sk.timak.plantio.model;

import java.time.ZonedDateTime;

public class RealTimeResponse {

    private ZonedDateTime time;

    public ZonedDateTime getTime() {
        return time;
    }

    public void setTime(ZonedDateTime time) {
        this.time = time;
    }
}
