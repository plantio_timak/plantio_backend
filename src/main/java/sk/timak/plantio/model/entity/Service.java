package sk.timak.plantio.model.entity;

import sk.timak.plantio.interfaces.ServiceType;

import javax.persistence.*;

@Entity(name="service")
public class Service {
    @Id
    private long id;

    @Enumerated(EnumType.STRING)
    private ServiceType name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(ServiceType name) {
        this.name = name;
    }

    public ServiceType getName() {
        return this.name;
    }
}
