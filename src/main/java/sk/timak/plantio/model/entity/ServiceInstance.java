package sk.timak.plantio.model.entity;


import javax.persistence.*;

@Entity(name="service_instance")
public class ServiceInstance {
    @Id
    private long id;

    private String ip;

    @Column(name="service_id")
    private long serviceId;

    private String name;

    public void setId(long id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public long getServiceId() {
        return this.serviceId;
    }

    public String getName() {
        return name;
    }
}
