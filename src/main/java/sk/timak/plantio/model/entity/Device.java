package sk.timak.plantio.model.entity;

import javax.persistence.*;

@Entity
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String type;

    private String plant;

    @Column(name = "use_interactive_mode")
    private boolean useInteractiveMode;

    @Column(name = "use_time_mode")
    private boolean useTimeMode;

    @Column(name = "time_mode_hours")
    private int timeModeHours;

    @Column(name = "watering_mode_id")
    private long wateringModeId;

    @Column(name = "service_instance_id")
    private long serviceInstanceId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public boolean isUseInteractiveMode() {
        return useInteractiveMode;
    }

    public void setUseInteractiveMode(boolean useInteractiveMode) {
        this.useInteractiveMode = useInteractiveMode;
    }

    public boolean isUseTimeMode() {
        return useTimeMode;
    }

    public void setUseTimeMode(boolean useTimeMode) {
        this.useTimeMode = useTimeMode;
    }

    public int getTimeModeHours() {
        return timeModeHours;
    }

    public void setTimeModeHours(int timeModeHours) {
        this.timeModeHours = timeModeHours;
    }

    public long getWateringModeId() {
        return wateringModeId;
    }

    public void setWateringModeId(long wateringModeid) {
        this.wateringModeId = wateringModeid;
    }

    public long getServiceInstanceId() {
        return serviceInstanceId;
    }

    public void setServiceInstanceId(long serviceInstanceId) {
        this.serviceInstanceId = serviceInstanceId;
    }
}
