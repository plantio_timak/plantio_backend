package sk.timak.plantio.model.entity;


import javax.persistence.*;

@Entity(name="user_service_instance")
public class UserServiceInstance {
    @Id
    private long id;

    @Column(name="user_id")
    private long userId;

    @Column(name="service_instance_id")
    private long serviceInstanceId;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getServiceInstanceId() {
        return serviceInstanceId;
    }

    public void setServiceInstanceId(long serviceInstanceId) {
        this.serviceInstanceId = serviceInstanceId;
    }
}
