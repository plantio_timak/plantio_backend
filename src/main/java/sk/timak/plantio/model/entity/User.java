package sk.timak.plantio.model.entity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@Entity(name="user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "username")
    private String userName;

    private String password;

    @Email
    private String email;

    @Column(name = "last_used_service_id")
    private long lastUsedServiceId;

    @Column(name = "last_used_service_instance_id")
    private long lastUsedServiceInstanceId;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getLastUsedServiceId() {
        return lastUsedServiceId;
    }

    public void setLastUsedServiceId(long lastUsedServiceId) {
        this.lastUsedServiceId = lastUsedServiceId;
    }

    public long getLastUsedServiceInstanceId() {
        return lastUsedServiceInstanceId;
    }

    public void setLastUsedServiceInstanceId(long lastUsedServiceInstanceId) {
        this.lastUsedServiceInstanceId = lastUsedServiceInstanceId;
    }
}
