package sk.timak.plantio.model.admin;

import sk.timak.plantio.model.room.Device;
import sk.timak.plantio.validators.ValidPlantioField;

import java.util.List;

public class CreateRoomRequest {

    @ValidPlantioField(mandatory = true)
    private long id;

    @ValidPlantioField(mandatory = true)
    private String ip;

    @ValidPlantioField(mandatory = true)
    private String name;

    @ValidPlantioField(mandatory = true)
    private List<Device> devices;

    @ValidPlantioField(mandatory = true)
    private String serviceType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
