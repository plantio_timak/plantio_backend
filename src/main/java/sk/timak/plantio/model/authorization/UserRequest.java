package sk.timak.plantio.model.authorization;

import sk.timak.plantio.validators.ValidPlantioField;

public class UserRequest{

    @ValidPlantioField(mandatory = true)
    private String userName;

    @ValidPlantioField(mandatory = true)
    private String password;

    @ValidPlantioField(mandatory = true)
    private String email;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
