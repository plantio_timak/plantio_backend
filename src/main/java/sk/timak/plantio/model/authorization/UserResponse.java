package sk.timak.plantio.model.authorization;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import sk.timak.plantio.interfaces.ServiceType;

import java.util.List;
import java.util.Map;

public class UserResponse {

    private String userName;

    @JsonIgnore
    private String password;

    private String token;

    private String email;

    private List<Flag> flags;

    private List<ServiceType> services;

    private String lastUsedService;

    private long lastUsedServiceInstance;

    private String sessionId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String usernmame) {
        this.userName = usernmame;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setServices(List<ServiceType> services) {
        this.services = services;
    }

    public List<ServiceType> getServices() {
        return this.services;
    }

    public List<Flag> getFlags() {
        return flags;
    }

    public void setFlags(List<Flag> flags) {
        this.flags = flags;
    }

    public String getLastUsedService() {
        return lastUsedService;
    }

    public void setLastUsedService(String lastUsedService) {
        this.lastUsedService = lastUsedService;
    }

    public long getLastUsedServiceInstance() {
        return lastUsedServiceInstance;
    }

    public void setLastUsedServiceInstance(long lastUsedServiceInstance) {
        this.lastUsedServiceInstance = lastUsedServiceInstance;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
