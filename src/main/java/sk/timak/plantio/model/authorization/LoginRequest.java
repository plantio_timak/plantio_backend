package sk.timak.plantio.model.authorization;

import sk.timak.plantio.validators.ValidPlantioField;

public class LoginRequest {

    @ValidPlantioField(mandatory = true)
    private String userName;

    @ValidPlantioField(mandatory = true)
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
