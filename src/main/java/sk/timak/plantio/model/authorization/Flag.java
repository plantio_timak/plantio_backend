package sk.timak.plantio.model.authorization;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Flag {
    IS_ADMIN ("isAdmin");

    private String value;

    private Flag(String value){this.value = value;}

    @JsonValue
    private String getValue(){return value;}

    @Override
    public String toString() {
        return getValue();
    }
}
