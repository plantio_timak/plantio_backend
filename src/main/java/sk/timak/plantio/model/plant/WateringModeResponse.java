package sk.timak.plantio.model.plant;

import java.util.List;

public class WateringModeResponse {

    public WateringModeResponse(List<WateringMode> wateringModes) {
        this.wateringModes = wateringModes;
    }

    private List<WateringMode> wateringModes;

    public List<WateringMode> getWateringModes() {
        return wateringModes;
    }

    public void setWateringModes(List<WateringMode> wateringModes) {
        this.wateringModes = wateringModes;
    }
}
