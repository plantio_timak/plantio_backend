package sk.timak.plantio.model.plant;

public enum PlantMode {
    TIME,
    INTERACTIVE
}
