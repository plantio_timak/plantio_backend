package sk.timak.plantio.model.plant;

import sk.timak.plantio.validators.ValidPlantioField;

public class ChangeWateringModeRequest {

    @ValidPlantioField(mandatory = true)
    private long wateringModeId;

    public long getWateringModeId() {
        return wateringModeId;
    }

    public void setWateringModeId(long wateringModeId) {
        this.wateringModeId = wateringModeId;
    }
}
