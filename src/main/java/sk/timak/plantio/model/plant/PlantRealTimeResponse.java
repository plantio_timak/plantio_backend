package sk.timak.plantio.model.plant;


import sk.timak.plantio.model.RealTimeResponse;

public class PlantRealTimeResponse extends RealTimeResponse {

    private PlantValues values;

    public PlantValues getValues() {
        return values;
    }

    public void setValues(PlantValues values) {
        this.values = values;
    }
}
