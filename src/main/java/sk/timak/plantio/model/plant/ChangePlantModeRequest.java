package sk.timak.plantio.model.plant;


public class ChangePlantModeRequest {

    private String plantMode;

    private int time;

    private long wateringModeId;

    public String getPlantMode() {
        return plantMode;
    }

    public void setPlantMode(String plantMode) {
        this.plantMode = plantMode;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public long getWateringModeId() {
        return wateringModeId;
    }

    public void setWateringModeId(long wateringModeId) {
        this.wateringModeId = wateringModeId;
    }
}
