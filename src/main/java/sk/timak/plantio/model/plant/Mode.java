package sk.timak.plantio.model.plant;


public class Mode {

    private PlantMode plantMode;

    private String time;

    public PlantMode getPlantMode() {
        return plantMode;
    }

    public void setPlantMode(PlantMode plantMode) {
        this.plantMode = plantMode;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
