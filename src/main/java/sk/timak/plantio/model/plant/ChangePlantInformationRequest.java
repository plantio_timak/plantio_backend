package sk.timak.plantio.model.plant;

import javax.validation.constraints.Size;

public class ChangePlantInformationRequest {

    @Size(max = 40, message = "FIELD_TOO_LONG")
    private String name;

    @Size(max = 40, message = "FIELD_TOO_LONG")
    private String plant;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }
}
