package sk.timak.plantio.model.plant;

import sk.timak.plantio.validators.ValidPlantioField;

import javax.validation.constraints.Size;

public class ChangeRoomInformationRequest {

    @ValidPlantioField(mandatory = true)
    @Size(max = 40, message = "FIELD_TOO_LONG")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
