package sk.timak.plantio.model.plant;

import java.util.List;

public class PlantsResponse {
    List<Plant> plants;

    public List<Plant> getPlants() {
        return plants;
    }

    public void setPlants(List<Plant> plants) {
        this.plants = plants;
    }
}
