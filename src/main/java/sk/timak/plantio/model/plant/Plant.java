package sk.timak.plantio.model.plant;

import java.util.List;

public class Plant {

    private long id;

    private String name;

    private String plant;

    private Mode mode;

    private WateringMode wateringMode;

    private List<PlantRealTimeResponse> plantValues;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public WateringMode getWateringMode() {
        return wateringMode;
    }

    public void setWateringMode(WateringMode wateringMode) {
        this.wateringMode = wateringMode;
    }

    public List<PlantRealTimeResponse> getPlantValues() {
        return plantValues;
    }

    public void setPlantValues(List<PlantRealTimeResponse> plantValues) {
        this.plantValues = plantValues;
    }
}
