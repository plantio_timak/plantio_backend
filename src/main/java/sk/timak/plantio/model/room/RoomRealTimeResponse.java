package sk.timak.plantio.model.room;

import sk.timak.plantio.model.RealTimeResponse;


public class RoomRealTimeResponse extends RealTimeResponse {

    private RoomValues values;

    public RoomValues getValues() {
        return values;
    }

    public void setValues(RoomValues values) {
        this.values = values;
    }
}
