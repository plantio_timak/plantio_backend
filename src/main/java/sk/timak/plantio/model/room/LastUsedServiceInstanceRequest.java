package sk.timak.plantio.model.room;

import sk.timak.plantio.validators.ValidPlantioField;

public class LastUsedServiceInstanceRequest {

    @ValidPlantioField(mandatory = true)
    private long lastUsedServiceInstanceId;

    public long getLastUsedServiceInstanceId() {
        return lastUsedServiceInstanceId;
    }

    public void setLastUsedServiceInstanceId(long lastUsedServiceInstanceId) {
        this.lastUsedServiceInstanceId = lastUsedServiceInstanceId;
    }
}
