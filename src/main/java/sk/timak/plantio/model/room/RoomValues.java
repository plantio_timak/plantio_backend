package sk.timak.plantio.model.room;

public class RoomValues {

    private double humidity;

    private double temperature;

    private boolean waterCapacity;

    private double lightIntensity;

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public boolean isWaterCapacity() {
        return waterCapacity;
    }

    public void setWaterCapacity(boolean waterCapacity) {
        this.waterCapacity = waterCapacity;
    }

    public double getLightIntensity() {
        return lightIntensity;
    }

    public void setLightIntensity(double lighIntensity) {
        this.lightIntensity = lighIntensity;
    }
}
