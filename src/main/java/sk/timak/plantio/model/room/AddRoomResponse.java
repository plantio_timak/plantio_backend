package sk.timak.plantio.model.room;

import java.util.List;

public class AddRoomResponse {

    private List<Long> rooms;

    public List<Long> getRooms() {
        return rooms;
    }

    public void setRooms(List<Long> rooms) {
        this.rooms = rooms;
    }
}
