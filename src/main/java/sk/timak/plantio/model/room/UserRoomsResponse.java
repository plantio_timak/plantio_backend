package sk.timak.plantio.model.room;

import java.util.List;


public class UserRoomsResponse {

    private List<UserRoom> userRooms;

    public List<UserRoom> getUserRooms() {
        return userRooms;
    }

    public void setUserRooms(List<UserRoom> userRooms) {
        this.userRooms = userRooms;
    }
}
