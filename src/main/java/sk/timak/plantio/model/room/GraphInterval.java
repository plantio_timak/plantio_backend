package sk.timak.plantio.model.room;

public enum GraphInterval {
    DAY,
    WEEK,
    MONTH
}
