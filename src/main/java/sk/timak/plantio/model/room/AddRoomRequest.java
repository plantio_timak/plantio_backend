package sk.timak.plantio.model.room;

import sk.timak.plantio.validators.ValidPlantioField;

public class AddRoomRequest {

    @ValidPlantioField(mandatory = true)
    private int deviceId;

    @ValidPlantioField(mandatory = true)
    private String serviceType;

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int id) {
        this.deviceId = id;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
