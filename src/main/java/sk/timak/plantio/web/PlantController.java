package sk.timak.plantio.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import sk.timak.plantio.model.plant.*;
import sk.timak.plantio.service.PlantService;
import sk.timak.plantio.web.path.Path;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(Path.WATERING)
public class PlantController {

    public static final String ONE_PLANT = "/my/plant/{plantId}";
    public static final String USER_PLANTS = "/my/plants/room/{roomId}";
    public static final String WATERING_MODES = "/watering-modes";
    public static final String CHANGE_PLANT_MODE = "my/plant/{plantId}/plant-mode";

    private final PlantService plantService;

    @Autowired
    public PlantController(PlantService plantService) {
        this.plantService = plantService;
    }

    @GetMapping(value = USER_PLANTS, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PlantsResponse> getUserPlants(HttpServletRequest request,
                                                        @PathVariable String roomId,
                                                        @RequestParam String interval) {
        return new ResponseEntity<>(plantService.getUserPlants(request, roomId, interval), HttpStatus.OK);
    }

    @GetMapping(value = WATERING_MODES, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<WateringModeResponse> getWateringModes() {
        return new ResponseEntity<>(plantService.getWateringModes(), HttpStatus.OK);
    }

    @PutMapping(value = CHANGE_PLANT_MODE, produces = APPLICATION_JSON_VALUE)
    public Plant changePlantModeForDevice(HttpServletRequest request,
                                                          @Valid @RequestBody ChangePlantModeRequest changePlantModeRequest,
                                                          Errors errors,
                                                          @PathVariable String plantId){
        return plantService.changePlantModeForDevice(request, changePlantModeRequest, plantId, errors);
    }

    @PutMapping(value = ONE_PLANT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Plant> changePlantInformation(HttpServletRequest request,
                                                        @PathVariable String plantId,
                                                        @Valid @RequestBody ChangePlantInformationRequest changePlantInformationRequest,
                                                        Errors errors) {
        return new ResponseEntity<>(plantService.changePlantInformation(request, plantId, changePlantInformationRequest, errors), HttpStatus.OK);
    }
}
