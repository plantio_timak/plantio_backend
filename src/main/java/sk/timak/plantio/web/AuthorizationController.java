package sk.timak.plantio.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import sk.timak.plantio.model.authorization.LoginRequest;
import sk.timak.plantio.model.authorization.UserRequest;
import sk.timak.plantio.model.authorization.UserResponse;
import sk.timak.plantio.service.UserService;
import sk.timak.plantio.web.path.Path;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static sk.timak.plantio.web.path.Path.AUTHORIZATION;
import static sk.timak.plantio.web.path.Path.REGISTRATION;

@RestController
@RequestMapping(Path.LOGIN)
public class AuthorizationController {


    private final UserService userService;

    @Autowired
    public AuthorizationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = REGISTRATION, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> registerOneUser(HttpServletRequest httpServletRequest, @Valid @RequestBody UserRequest userRequest, Errors errors) {
        return new ResponseEntity<>(userService.registerOneUser(httpServletRequest, userRequest, errors), HttpStatus.OK);
    }

    @PostMapping(value = AUTHORIZATION, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> loginUser(HttpServletRequest httpServletRequest, @Valid @RequestBody LoginRequest loginRequest, Errors errors) {
        return new ResponseEntity<>(userService.loginUser(httpServletRequest, loginRequest, errors), HttpStatus.OK);
    }
}
