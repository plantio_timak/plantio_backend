package sk.timak.plantio.web;

import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import sk.timak.plantio.model.admin.CreateRoomRequest;
import sk.timak.plantio.service.AdminService;
import sk.timak.plantio.web.path.Path;


import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(Path.ADMIN)
public class AdminController {

    private AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping(value = Path.CREATE_ROOM, produces = APPLICATION_JSON_VALUE)
    public void createRoomForDevice(@Valid @RequestBody CreateRoomRequest createRoomRequest, Errors errors) {
        adminService.createRoomWithDevicesForUser(createRoomRequest, errors);
    }
}
