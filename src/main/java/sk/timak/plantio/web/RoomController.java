package sk.timak.plantio.web;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import sk.timak.plantio.model.plant.ChangeRoomInformationRequest;
import sk.timak.plantio.model.room.*;
import sk.timak.plantio.service.RoomService;
import sk.timak.plantio.service.UserService;
import sk.timak.plantio.web.path.Path;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(Path.WATERING)
public class RoomController {

    public static final String MY_ROOM = "/my/room/{roomId}";
    public static final String ADD_ROOM_TO_USER = "/my/room/add";
    public static final String USER_ROOMS = "/my/rooms";
    public static final String SET_USER_LAST_USED_SERVICE_INSTNACE = "/my/rooms/last-used/set";

    private final RoomService roomService;

    private final UserService userService;

    public RoomController(RoomService roomService, UserService userService) {
        this.roomService = roomService;
        this.userService = userService;
    }

    @GetMapping(value = MY_ROOM, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RoomRealTimeResponse>> getRoomInformation(HttpServletRequest request,
                                                                         @PathVariable String roomId,
                                                                         @RequestParam String interval,
                                                                         @RequestParam String serviceType) {
        return new ResponseEntity<>(roomService.getRoomInformation(request, roomId, interval, serviceType), HttpStatus.OK);
    }

    @PostMapping(value = ADD_ROOM_TO_USER, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserRoomsResponse> addRoomToUser(HttpServletRequest request,
                                                           @Valid @RequestBody AddRoomRequest addRoomRequest,
                                                           Errors errors) {
        return new ResponseEntity<>(roomService.addRoomToUser(request, addRoomRequest, errors), HttpStatus.OK);
    }

    @GetMapping(value = USER_ROOMS, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserRoomsResponse> getUserRooms(HttpServletRequest request, @RequestParam String serviceType) {
        return new ResponseEntity<>(roomService.getUserRooms(request, serviceType), HttpStatus.OK);
    }

    @PutMapping(value = SET_USER_LAST_USED_SERVICE_INSTNACE, produces = APPLICATION_JSON_VALUE)
    public void setUserLastUsedServiceInstance(HttpServletRequest request,
                                               @Valid @RequestBody LastUsedServiceInstanceRequest serviceInstanceRequest,
                                               Errors errors) {
        userService.setUserLastUsedServiceInstance(request, serviceInstanceRequest, errors);
    }

    @PutMapping(value = MY_ROOM, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserRoomsResponse> changeUserRoomInformation(HttpServletRequest request,
                                                                       @PathVariable String roomId,
                                                                       @RequestParam String serviceType,
                                                                       @Valid @RequestBody ChangeRoomInformationRequest changeRoomInformationRequest,
                                                                       Errors errors
    ) {
        return new ResponseEntity<>(roomService.changeRoomInformation(request, roomId, changeRoomInformationRequest, serviceType, errors), HttpStatus.OK);
    }
}
