package sk.timak.plantio.web.path;

public interface Path {
    String LOGIN = "/api/login";
    String ADMIN = "/api/admin";
    String WATERING = "/api/watering";
    String REAL_TIME = "/api/real-time/";

    String AUTHORIZATION = "/authorization";
    String REGISTRATION = "/registration";
    String CREATE_ROOM = "/room/add";
}
