package sk.timak.plantio.conf.mqtt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import sk.timak.plantio.impl.PlantServiceImpl;
import sk.timak.plantio.model.entity.Device;
import sk.timak.plantio.model.entity.WateringMode;
import sk.timak.plantio.model.plant.PlantMode;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Service
public class MqttConnection {

    private Logger LOGGER = LoggerFactory.getLogger(MqttConnection.class);

    private MqttAsyncClient mqttClient;
    private static final String URL_TO_MQTT_BROKER = "tcp://mqtt:3000";

    @Autowired
    private PlantServiceImpl plantServiceImpl;

    @Bean
    public void createConnectionToMqttBroker() {

        try {
            mqttClient = new MqttAsyncClient(URL_TO_MQTT_BROKER, UUID.randomUUID().toString(), new MemoryPersistence());
            MqttHandler callback = new MqttHandler();
            mqttClient.setCallback(callback);
        } catch (MqttException e){
            LOGGER.info("Error while creating MqttAsyncClient : " + e);
        }

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(true);
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setKeepAliveInterval(100);

        try{
            mqttClient.connect(mqttConnectOptions, null).waitForCompletion();
            subscribe(MqttConstants.PLANT_STATE_TOPIC);
            subscribe(MqttConstants.ROOM_STATE_TOPIC);
        } catch (Exception e) {
            LOGGER.info("Error while connecting to MQTT broker : " + e);
        }
    }

    private void subscribe(String topic) {
        if(mqttClient.isConnected()) {
            try {
                mqttClient.subscribe(topic, 0);
            } catch (MqttException e) {
                LOGGER.info("Error while creating Subscribe for " + topic + " topic.");
            }
        }
    }

    public void publish(String topic, MqttMessage message){
        if(mqttClient.isConnected()) {
                Thread thread = new Thread() {
                    public void run() {
                        try {
                            mqttClient.publish(topic, message);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
        }else{
            System.out.println("Client is not connected");
        }
    }

    //String deviceID, String planter,String plantName, String mode, String configParam
    public MqttMessage createMqttMessage(Device device, String mode){
        MqttMessage message = new MqttMessage();

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();

        rootNode.put("deviceID", Long.toString(device.getId()));
        rootNode.put("mode", mode);

        if(mode.equals(PlantMode.INTERACTIVE.name())) {
            WateringMode wateringMode = plantServiceImpl.getWateringModeOfDeviceById(device.getWateringModeId());
            rootNode.put("min_soil_humidity", Integer.toString(wateringMode.getMinSoilHumidity()));
        } else {
            rootNode.put("time", Integer.toString(device.getTimeModeHours()));
        }

        try {
            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
            message.setQos(0);
            message.setPayload(jsonString.getBytes(StandardCharsets.UTF_8));
            return message;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
