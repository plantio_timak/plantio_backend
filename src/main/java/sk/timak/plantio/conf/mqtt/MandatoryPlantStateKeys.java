package sk.timak.plantio.conf.mqtt;

public enum MandatoryPlantStateKeys implements MandatoryKeys {
    // values will be probably extended in the further process
    MEASUREMENT("measurement"),
    TIME("time"),
    FIELDS("fields"),
    PLANT_ID("plant_id"),
    TEMPERATURE("temperature"),
    HUMIDITY("humidity");

    private final String mandatory_key;

    MandatoryPlantStateKeys(String key) {
        this.mandatory_key = key;
    }

    public String getMandatoryKey() {
        return mandatory_key;
    }
}

