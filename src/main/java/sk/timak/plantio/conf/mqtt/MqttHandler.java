package sk.timak.plantio.conf.mqtt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.timak.plantio.influxdb.InfluxDbHandler;

public class MqttHandler implements MqttCallback{

    private Logger LOGGER = LoggerFactory.getLogger(MqttHandler.class);

    @Override
    public void connectionLost(Throwable throwable) {
        LOGGER.info("Connection was lost with message : " + throwable.getMessage());
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        JsonNode parsedMqttMessageToJson = null;

        if(topic.equals(MqttConstants.PLANT_STATE_TOPIC)) {
            parsedMqttMessageToJson = parseAndValidateMqttMessage(message, MandatoryPlantStateKeys.values());
        }
        else if(topic.equals(MqttConstants.ROOM_STATE_TOPIC)) {
            parsedMqttMessageToJson = parseAndValidateMqttMessage(message, MandatoryRoomStateKeys.values());
        }

        if(parsedMqttMessageToJson != null){
            InfluxDbHandler influxDbHandler = new InfluxDbHandler();
            influxDbHandler.insertDataToInfluxDatabase(parsedMqttMessageToJson);
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        try {
            LOGGER.info("Delivery was complete " + new String(iMqttDeliveryToken.getMessage().getPayload()));
        } catch (MqttException e) {
            LOGGER.info("Process failed with exception : " + e);
        }
    }

    private JsonNode parseAndValidateMqttMessage(MqttMessage message, Object[] mandatoryKeys) {
        JsonNode jsonObj;

        // parsing MQTT message to JSON object
        try {
            ObjectMapper mapper = new ObjectMapper();
            jsonObj = mapper.readTree(new String(message.getPayload()).replaceAll("\\s+", ""));
        } catch (JsonProcessingException e){
            LOGGER.info("Invalid MQTT message for further processing with InfluxDB : " + e);
            return null;
        }

        // validating JSON object if it includes all necessary parameters for further processing with InfluxDB
        for (Object key : mandatoryKeys) {
            if(jsonObj.findValue(((MandatoryKeys) key).getMandatoryKey()) == null ){
                LOGGER.info("JSON is not including mandatory parameter : " + ((MandatoryKeys) key).getMandatoryKey() + " parameter was not found." );
                return null;
            }
        }

        return jsonObj;
    }
}
