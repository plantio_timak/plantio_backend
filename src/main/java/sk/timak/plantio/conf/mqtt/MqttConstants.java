package sk.timak.plantio.conf.mqtt;

public class MqttConstants {

    public static final String PLANT_STATE_TOPIC = "topic/plantio/plant_state";
    public static final String ROOM_STATE_TOPIC = "topic/plantio/room_state";
    public static final String WATER_MODE_TOPIC = "topic/plantio/water_mode";
}
