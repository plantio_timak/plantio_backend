package sk.timak.plantio.conf.mqtt;

public interface MandatoryKeys {

    String getMandatoryKey();
}
