package sk.timak.plantio.conf.mqtt;

public enum MandatoryRoomStateKeys implements MandatoryKeys{

    // values will be probably extended in the further process
    MEASUREMENT("measurement"),
    TIME("time"),
    FIELDS("fields"),
    ROOM_ID("room_id"),
    TEMPERATURE("temperature"),
    HUMIDITY("humidity"),
    LIGHT_INTESITY("light_intensity");

    private final String mandatory_key;

    MandatoryRoomStateKeys(String key) {
        this.mandatory_key = key;
    }

    public String getMandatoryKey() {
        return mandatory_key;
    }
}
