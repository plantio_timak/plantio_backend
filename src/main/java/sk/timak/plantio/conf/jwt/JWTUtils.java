package sk.timak.plantio.conf.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import sk.timak.plantio.conf.SecurityConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

import static sk.timak.plantio.conf.SecurityConstants.*;

public class JWTUtils {

    public static String generateTokenAfterSuccessLogin(String userName) {
        boolean admin = false;

        if (userName.contains(ADMIN)) {
            admin = true;
        }
        return TOKEN_PREFIX + JWT.create()
                .withSubject(userName)
                .withClaim(ADMIN, admin)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
    }

    public static String getUserNameFromRequest(HttpServletRequest request) {
        String token = request.getHeader(SecurityConstants.HEADER_STRING).replace(SecurityConstants.TOKEN_PREFIX,"");
        return JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getSubject();
    }

    public static boolean isRoleAdminUser(String token) {
        return JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getClaim(ADMIN).asBoolean();
    }

    public static String getSessionIdFromRequest(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getSession().getId();
    }

}