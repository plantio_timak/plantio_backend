package sk.timak.plantio.conf.jwt;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import sk.timak.plantio.exception.TokenInvalidException;
import sk.timak.plantio.web.path.Path;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static sk.timak.plantio.conf.SecurityConstants.*;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter() {
        super(authentication -> authentication);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String tokenHeader = req.getHeader(HEADER_STRING);

        if (tokenHeader == null || !tokenHeader.startsWith(TOKEN_PREFIX)) {
            if (req.getServletPath().equals(Path.LOGIN + Path.AUTHORIZATION) || req.getServletPath().equals(Path.LOGIN + Path.REGISTRATION) || req.getServletPath().equals(Path.REAL_TIME)) {
                chain.doFilter(req, res);
            } else {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
            }
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = req.getHeader(HEADER_STRING);

        if (JWTUtils.isRoleAdminUser(token) && req.getServletPath().contains(Path.ADMIN)) {
            chain.doFilter(req, res);
        } else if (!JWTUtils.isRoleAdminUser(token) && req.getServletPath().contains(Path.WATERING)) {
            chain.doFilter(req, res);
        }
        else throw new TokenInvalidException("token");

        if (res.getContentType() == null || res.getContentType().equals("")) {
            res.setStatus(HttpStatus.NO_CONTENT.value());
        }
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);

        try {
            if (token != null) {
                String user = JWTUtils.getUserNameFromRequest(request);
                if (user != null) {
                    return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
                }
            }
        } catch (Exception e) {
            throw new TokenInvalidException("token");
        }
        return null;
    }
}