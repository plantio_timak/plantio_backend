package sk.timak.plantio.conf;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@Import({WebConfiguration.class, WebSecurityConfiguration.class})
@EnableJpaRepositories(value = "sk.timak.plantio.persistance")
@EntityScan("sk.timak.plantio.model")
public class ApplicationContext {
}
