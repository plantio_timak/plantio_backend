package sk.timak.plantio.conf;

public class SecurityConstants {

    public static final String SECRET = "SECRET_KEY";
    public static final long EXPIRATION_TIME = 1_800_000; // 30 mins
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "authorization";
    public static final String ADMIN = "admin";
    public static final String X_SESSION_ID = "X-Session-Id";

}