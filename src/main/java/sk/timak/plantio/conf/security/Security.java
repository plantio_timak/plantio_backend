package sk.timak.plantio.conf.security;


import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class Security {
    private static String sha256hex;

    public static String hash(String password) {
        sha256hex = Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
        return sha256hex;
    }
}

