package sk.timak.plantio.conf.filter;


import org.slf4j.MDC;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static sk.timak.plantio.conf.SecurityConstants.*;


public class MDCFilter extends OncePerRequestFilter {


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        insertIntoMDC(request);
        try {
            filterChain.doFilter(request, response);
        } finally {
            clearMDC();
        }
    }

    private void insertIntoMDC(HttpServletRequest request) {
        final String token = request.getHeader(X_SESSION_ID);
        if (token != null) {
            MDC.put(X_SESSION_ID, token);
        }
    }

    private void clearMDC() {
        MDC.remove(X_SESSION_ID);
    }
}