1. nainstalovat openJDK 14 https://jdk.java.net/14/
2. nakonfigurovat enviromental variables path na noju javu
3. refresh cez maven
4. aplikacia sa da BUILDIt na lavom kraji na zalozke maven pomocou spustenia prikazov clean install
5. aplikaciu spustis nastavenim spring boot initializera, spring ma ebmedovany tomcat a mozes si prevolavat EP


->rozbehanie cez docker
1. nainstalovat docker
2.tento navodi mi pomohol https://medium.com/@por.porkaew15/spring-boot-application-and-docker-with-intellij-ide-8d2a843c529e
    1. mvn package
    2. docker build -t "plantio_backend_docker" .
    3. docker images
    4. docker tag <imageID(z 3.kroku)> <hub_username>/plantio_backend_docker
    5. docker push <hub_username>/plantio_backend_docker
    6. docker-compose up -d
    7. docker ps
3. databaza bude zle nastavena, nechce zobrat auto_increment  =>> https://dev.to/n350071/login-to-mysql-on-docker-and-run-a-sql-file-2bk7
    1.$ docker cp plantio.sql(je v projekte pri pom.xml) [contaier-id(tu vyber id mysql DB)]:/nazovakychcesnadockri.sql
    2.$ docker ps
      $ docker exec -it [container-id] /bin/bash
    3.$ mysql -u root -p
      $ show databases;
      $ use [plantIO_DB];
      $ show tables;
    4.$ source nazovakychcesnadockri.sql
4.  describe user;
5. ak vidis ze v popise nema id pri extra auto_increment tak vykonaj tieto prikazy:
    1.SET FOREIGN_KEY_CHECKS = 0;
    2. ALTER TABLE user MODIFY COLUMN id INT AUTO_INCREMENT;
    3.SET FOREIGN_KEY_CHECKS = 1;
6. skontroluj pomocou describe user; prikazu ci sa pridalo do extra auto increment a malo by to fachat

--------------------------docker-compose dev---------------------------------------
docker-compose dev prikaz:
docker-compose -f docker-compose.dev.yml up
zbuildi vsetky servicy na dockri
+phpmyadmin na localhost:8080
